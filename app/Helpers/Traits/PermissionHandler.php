<?php
namespace App\Helpers\Traits;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Spatie\Permission\Exceptions\UnauthorizedException;

trait PermissionHandler {

	/**
	* Check if user can do some action, else throw UnauthorizedException
	* @param Illuminate\Http\Request
	* @param permission
	* @return Illuminate\Auth\AuthenticationException
	* @return Spatie\Permission\Exceptions\UnauthorizedException
	*/
	public function can(Request $request,$permission){
		$user = $request->user();
		if(!$user){
			throw new AuthenticationException;
		}
        if(!$user->can($permission)){
        	throw new UnauthorizedException(403,"You have not permission to do that action");
        }
	}
}
?>
