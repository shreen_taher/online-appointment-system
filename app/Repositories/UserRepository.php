<?php

namespace App\Repositories;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class UserRepository
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    function save($data)
    {
        $obj = array_merge(Arr::except($data, 'password', 'appointments'),
            [
                'password' => bcrypt($data['password']),
                'host_id' => auth()->id(),
            ]);

        $user = $this->user->create($obj);

        $appointments = array_map(function($appointment)
        {
            $appointment['meeting_date'] = Carbon::parse($appointment['meeting_date']);
            return $appointment;

        }, $data['appointments']);

        $user->hostAppointments()->createMany($appointments);

        return $user;
    }

    function find($id)
    {

        return $this->user->find($id);
    }

    function findAll()
    {
        return $this->user->where('role',2)->get();
    }

    function update($id, $data)
    {
        $user = $this->find($id);

        $user->update($data);

        return $user;
    }

    function delete($id)
    {
        $this->user->destroy($id);
    }
}
