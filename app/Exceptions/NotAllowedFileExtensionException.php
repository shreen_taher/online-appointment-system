<?php

namespace App\Exceptions;

use Exception;

class NotAllowedFileExtensionException extends Exception
{
    use ExceptionTrait;

    public function render(){

    	return $this->renderException("NotAllowedFileExtensionException","Not allowed file extension",501);
    }
}
