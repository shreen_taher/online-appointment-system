<?php

namespace App\Exceptions;

use Exception;

class LoginFailedException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
    	return $this->renderException("LoginFailedException","Email or password wrong,please try again",401);
    }
}
