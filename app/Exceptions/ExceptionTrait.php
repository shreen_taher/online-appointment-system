<?php
namespace App\Exceptions;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Validation\ValidationException;
// use Spatie\Permission\Exceptions\PermissionAlreadyExists;
// use Spatie\Permission\Exceptions\RoleAlreadyExists;
// use Spatie\Permission\Exceptions\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait ExceptionTrait{

        /**
        * Render exception as HTTP response json object
        * @param  \Illuminate\Http\Request  $request
        * @param  \Exception  $exception
        * @return \Illuminate\Http\Response
        */

	public function apiException($request,Exception $exception){

        /** Handle ModelNotFoundException */
	if($exception instanceof ModelNotFoundException){
		return $this->renderException('ModelNotFoundException',$exception->getMessage(),Response::HTTP_NOT_FOUND);
        }

        /** Handle NotFoundHttpException */
        if ($exception instanceof NotFoundHttpException) {
        	return $this->renderException('NotFoundHttpException',"Route Found",Response::HTTP_NOT_FOUND);
        }

        /** Handle AuthenticationException */
        if($exception instanceof AuthenticationException) {
        	return $this->renderException('AuthenticationException','Unauthorized Action',Response::HTTP_UNAUTHORIZED);
        }

        /** Handle UnauthorizedException */
        // if($exception instanceof UnauthorizedException) {
        //         return $this->renderException('UnauthorizedException',$exception->getMessage(),$exception->getStatusCode());
        // }

        /** Handle QueryException */
        if($exception instanceof QueryException){
                return $this->renderException('QueryException',$exception->getMessage(),Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        /** Handle RoleAlreadyExists */
        // if($exception instanceof RoleAlreadyExists){
        //         return $this->renderException('RoleAlreadyExists',$exception->getMessage(),Response::HTTP_INTERNAL_SERVER_ERROR);
        // }

        /** Handle PermissionAlreadyExists */
        // if($exception instanceof PermissionAlreadyExists){
        //         return $this->renderException('PermissionAlreadyExists',$exception->getMessage(),Response::HTTP_INTERNAL_SERVER_ERROR);
        // }

        /** Handle MethodNotAllowedHttpException */
        if($exception instanceof MethodNotAllowedHttpException){
                return $this->renderException('MethodNotAllowedHttpException',"Method Not Allowed Http",Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        /** Handle ValidationException */
        if($exception instanceof  ValidationException){

        	/** Start convert validation errors array to string. */
        	$errors = [];
        	if(count($exception->errors())){
        		foreach ($exception->errors() as $field => $err) {
        			if(count($err)){
        				foreach ($err as $key => $value) {
        					$errors[] = $value;
        				}
        			}
        		}
        	}
        	$errorsString = implode('', $errors);
        	/** End*/

        	return $this->renderException(' ValidationException',$errorsString,Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return parent::render($request, $exception);
	}

	/**
	* Render Exception as json object
	* @param $exceptionName,$exceptionMessage,$statusCode
	* @return \Illuminate\Http\Response
	*/
	public function renderException($exceptionName,$exceptionMessage,$statusCode)
	{
		$exceptionJson['statusCode'] = $statusCode;
		$exceptionJson['error']['name'] = $exceptionName;
		$exceptionJson['error']['message'] = $exceptionMessage;
		return response()->json($exceptionJson,$statusCode);
	}
}
?>
