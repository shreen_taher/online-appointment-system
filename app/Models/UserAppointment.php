<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserAppointment extends Model
{
    protected $fillable = ['meeting_date', 'start_time', 'end_time', 'title', 'host_id', 'attendee_id'];

    public function host()
    {
       return $this->belongsTo(User::class, 'host_id');
    }

    public function attendee()
    {
       return $this->belongsTo(User::class, 'attendee_id');
    }

}
