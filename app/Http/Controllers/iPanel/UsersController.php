<?php

namespace App\Http\Controllers\iPanel;

use App\Http\Controllers\Controller;
use App\Helpers\Traits\RESTApi;
use App\Http\Requests\HostRequest;
use App\Http\Resources\UserResource;
use App\Repositories\UserRepository;

class UsersController extends Controller
{
    use RESTApi;

    private $userRepository;

    public function __construct(UserRepository $repository)
    {

        $this->userRepository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->userRepository->findAll();

        return $this->sendJson(UserResource::collection($users));
    }


    public function store(HostRequest $request)
    {
       $user = $this->userRepository->save($request->all());

       return $this->sendJson(new UserResource($user));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->userRepository->delete($id);

        return $this->sendJson(null);
    }

}
