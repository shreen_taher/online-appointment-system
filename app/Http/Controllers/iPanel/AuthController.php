<?php

namespace App\Http\Controllers\iPanel;

use App\Exceptions\LoginFailedException;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Helpers\Traits\RESTApi;
use App\Http\Requests\RegisterRequest;
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use RESTApi;

    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if ($token = auth('api')->attempt($credentials, $request->has('remember'))) {

                return $this->respondWithToken($token);

        }else{

            throw new LoginFailedException();

        }

    }

    public function register(RegisterRequest $request)
    {
        $data = array_merge($request->except('password'),
            ['password' => bcrypt($request->password)]);

        $user = User::create($data);

        $token = auth('api')->login($user);

        if ($token) {

                return $this->respondWithToken($token);

        }else{

            throw new LoginFailedException();

        }

    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();
        return $this->sendJson(null);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $response = [
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => auth()->user(),
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ];
        return $this->sendJson($response);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('api');
    }
}
