<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(Auth::user()->can('manage_experiments'));
        if (Auth::guard('web')->guest()) {

            return redirect('/')->with('error','يجب ان تقوم بتسجيل الدخول أولا  .');
        }

        if (Auth::check() && Auth::user()->role != 1)
            abort(404);

        return $next($request);

        // if (Auth::check() &&  (
        // Auth::user()->hasRole('administrator')
        // || Auth::user()->hasRole('experiment_prs')
        // || Auth::user()->hasRole('samples_prs')
        // || Auth::user()->hasRole('experiment_author')
        // || Auth::user()->hasRole('experiment_maintainer')
        // || Auth::user()->hasRole('products_prs')

        // )) {
        //     return $next($request);
        // }
        // return redirect()->route('admin.login');

    }
}
