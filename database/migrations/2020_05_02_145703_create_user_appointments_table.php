<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->date('meeting_date');
            $table->time('start_time');
            $table->time('end_time');

            $table->unsignedInteger('host_id');
            $table->unsignedInteger('attendee_id')->nullable();

            $table->foreign('host_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('attendee_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_appointments');
    }
}
