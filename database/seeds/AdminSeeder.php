<?php

use App\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'password' => bcrypt("mxO!slZMs"),

        User::create([
            'first_name' => 'Adminstratore',
            'last_name' => 'System',
            'email' => "admin@gmail.com",
            'password' => bcrypt("123456"),
            'role' => 1,
        ]);
    }
}
