<?php

use App\Http\Controllers\iPanel\ProductMovementsController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::group(['prefix' => LaravelLocalization::setLocale()], function() {

Route::group(['prefix' => 'iPanel'], function () {

    Route::post('login', 'iPanel\AuthController@login');

    Route::post('register', 'iPanel\AuthController@register');

    Route::get('locales', 'iPanel\LanguagesController@locales');

});

Route::group(['prefix' => 'iPanel', 'middleware' => ['auth:api']], function () {

    Route::post('logout', 'iPanel\AuthController@logout');
    Route::post('refresh', 'iPanel\AuthController@refresh');

});

Route::group([
    'prefix' => 'iPanel',
    'middleware' => ['auth:api']
    ], function () {

    Route::get('set-locale/{locale}', 'iPanel\LanguagesController@switchLang');

    Route::apiResources([
        'users'             => 'iPanel\UsersController',
        ]);

});
