<?php
return [
    'categories'=>"categories",
    'subcategories'=> "subcategories",
    'groups'=> "groups",
    'producerCompanies'=> "producer companies",
    'distributedCompanies'=> "distributed companies",
    'branches'=> "branches",
    'clients'=> "clients",
    'products'=> "products",
    'models'=> "models",
    'experiments'=> "experiments",
    'QuantityExceededException' => "Not quantity Enough, allowed quantity is ",
];
