<?php
return [
    'categories' => "الاقسام الرئيسية",
    'subcategories' => "الاقسام الفرعية",
    'groups' => "المجموعات",
    'producerCompanies' => "الشركات المنتجه",
    'distributedCompanies' => "الشركات الموزعه",
    'branches' => "الفروع",
    'clients' => "العملاء",
    'products' => "الاصناف",
    'models' => "النماذج",
    'experiments' => "التجارب",
    'QuantityExceededException' => "لا يوجد كميه متاحه, الكمية المتوفره هيا ",

];
