<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    "accepted" => "ﻳﺠﺐ ﻗﺒﻮﻝ :attribute.",
    "active_url" => "اﻝ :attribute ﻟﻴﺲ ﺭاﺑﻂ ﺻﺤﻴﺢ.",
    "after" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺗﺎﺭﻳﺦ ﺑﻌﺪ :date.",
    "alpha" => "ﻳﺠﺐ ﺃﻥ ﻳﺤﺘﻮﻱ :attribute ﻋﻠﻰ ﺣﺮﻭﻑ ﻓﻘﻂ.",
    "alpha_dash" => "ﻳﺠﺐ ﺃﻥ ﻳﺤﺘﻮﻱ :attribute ﻋﻠﻰ ﺣﺮﻭﻑ, ﻭ ﺃﺭﻗﺎﻡ ﻭ ﺷﺮﻃﺎﺕ ﻓﻘﻂ.",
    "alpha_num" => "ﻳﺠﺐ ﺃﻥ ﻳﺤﺘﻮﻱ :attribute ﻋﻠﻰ ﺣﺮﻭﻑ, ﻭ ﺃﺭﻗﺎﻡ.",
    "array" => "ﻳﺠﺐ ﺃﻥ ﺗﻜﻮﻥ :attribute ﻣﺠﻤﻮﻋﺔ ﻓﻘﻂ.",
    "before" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺗﺎﺭﻳﺦ ﻗﺒﻞ :date.",
    "before_or_equal" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺗﺎﺭﻳﺦ ﻗﺒﻞ او يساوى :date.",
    "after_or_equal" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺗﺎﺭﻳﺦ بعد او يساوى :date.",
    "between" => array(
        "numeric" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺑﻴﻦ :min ﻭ :max.",
        "file" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺑﻴﻦ :min ﻭ :max ﻛﻴﻠﻮﺑﺎﻳﺖ.",
        "string" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺑﻴﻦ :min ﻭ :max ﺣﺮﻑ.",
        "array" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺑﻴﻦ :min ﻭ :max ﻋﻨﺼﺮ.",
    ),
    "confirmed" => "ﺗﺄﻛﻴﺪ :attribute ﻻ ﻳﺘﻄﺎﺑﻖ.",
    "date" => ":attribute ﻟﻴﺲ ﺗﺎﺭﻳﺦ ﺻﺎﻟﺢ.",
    "date_format" => ":attribute ﻻ ﺗﻄﺎﺑﻖ اﻟﺸﻜﻞ :format.",
    "different" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﻭ :other ﻣﺨﺘﻠﻔﺎﻥ.",
    "digits" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute :digits.",
    "digits_between" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺑﻴﻦ :min ﻭ :max.",
    "email" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺑﺮﻳﺪ ﺇﻟﻜﺘﺮﻭﻧﻲ ﺻﺤﻴﺢ.",
    "exists" => ":attribute ﻏﻴﺮ ﺻﺤﻴﺢ.",
    "image" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺻﻮﺭﺓ.",
    "in" => ":attribute ﻏﻴﺮ ﺻﺤﻴﺢ.",
    "integer" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﻋﺪﺩ ﺻﺤﻴﺢ.",
    "ip" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﻋﻨﻮاﻥ ﺟﻬﺎﺯ ﺻﺤﻴﺢ.",
    "max" => array(
        "numeric" => "ﻳﺠﺐ ﺃﻻ ﻳﻜﻮﻥ :attribute ﺃﻛﺒﺮ ﻣﻦ :max.",
        "file" => "ﻳﺠﺐ ﺃﻻ ﻳﻜﻮﻥ :attribute ﺃﻛﺒﺮ ﻣﻦ :max ﻛﻴﻠﻮﺑﺎﻳﺖ.",
        "string" => "ﻳﺠﺐ ﺃﻻ ﻳﻜﻮﻥ :attribute ﺃﻛﺒﺮ ﻣﻦ :max ﺣﺮﻑ.",
        "array" => "ﻳﺠﺐ ﺃﻻ ﻳﻜﻮﻥ :attribute ﺃﻛﺒﺮ ﻣﻦ :max ﻋﻨﺼﺮ.",
    ),
    "mimes" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﻣﻠﻒ ﻣﻦ ﻧﻮﻉ: :values.",
    "min" => array(
        "numeric" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﻋﻠﻰ اﻷﻗﻞ :min.",
        "file" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﻋﻠﻰ اﻷﻗﻞ :min ﻛﻴﻠﻮﺑﺎﻳﺖ.",
        "string" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﻋﻠﻰ اﻷﻗﻞ :min ﺣﺮﻑ.",
        "array" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﻋﻠﻰ اﻷﻗﻞ :min ﻋﻨﺼﺮ.",
    ),
    "not_in" => ":attribute اﻟﻤﺨﺘﺎﺭ ﻟﻴﺲ ﺻﺤﻴﺢ.",
    "numeric" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺭﻗﻢ.",
    "regex" => "ﺷﻜﻞ :attribute ﻏﻴﺮ ﺻﺤﻴﺢ.",
    "required" => "يجب ملء حقل :attribute.",
    "required_if" => "ﺤﻘﻞ :attribute ﻣﻄﻠﻮﺏ ﺇﺩﺧﺎﻟﻪ.",
    "required_with" => "ﺤﻘﻞ :attribute ﻣﻄﻠﻮﺏ ﺇﺩﺧﺎﻟﻪ ﻋﻨﺪﻣﺎ ﻳﻜﻮﻥ :values ﻣﺘﻮاﺟﺪ.",
    "required_with_all" => "ﺤﻘﻞ :attribute ﻣﻄﻠﻮﺏ ﺇﺩﺧﺎﻟﻪ ﻋﻨﺪﻣﺎ ﻳﻜﻮﻥ :values ﻣﺘﻮاﺟﺪ.",
    "required_without" => "ﺤﻘﻞ :attribute ﻣﻄﻠﻮﺏ ﺇﺩﺧﺎﻟﻪ ﻋﻨﺪﻣﺎ ﻳﻜﻮﻥ :values ﻏﻴﺮ ﻣﺘﻮاﺟﺪ.",
    "required_without_all" => "ﺤﻘﻞ :attribute ﻣﻄﻠﻮﺏ ﺇﺩﺧﺎﻟﻪ ﻋﻨﺪﻣﺎ ﻻ ﻳﻜﻮﻥ اﻱ ﻣﻦ :values ﻣﺘﻮاﺟﺪ.",
    "same" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﻭ :other ﻣﺘﻄﺎﺑﻘﺎﻥ.",
    "size" => array(
        "numeric" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺑﺤﺠﻢ :size.",
        "file" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺑﺤﺠﻢ :size ﻛﻴﻠﻮﺑﺎﻳﺖ.",
        "string" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺑﺤﺠﻢ :size ﺣﺮﻑ.",
        "array" => "ﻳﺠﺐ ﺃﻥ ﻳﻜﻮﻥ :attribute ﺑﺤﺠﻢ :size ﻋﻨﺼﺮ.",
    ),
    "unique" => "ﻟﻘﺪ ﺗﻢ ﺇﺩﺧﺎﻝ :attribute ﻣﻤﺎﺛﻞ ﻣﺴﺒﻘﺎ.",
    'url' => 'صيغة رابط :attribute غير صحيحة',
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => [
        'ar.name' => [
            'required' => 'الاسم الأول مطلوب',
        ],
        'en.name' => [
            'required' => 'الاسم الأخير مطلوب',
        ],
        'dob' => [
            'required' => 'تاريخ الميلاد مطلوب',
        ],
        'age' => [
            'required' => 'العمر مطلوب',
        ],
        'mobile' => [
            'required' => 'رقم الجوال مطلوب',
        ],
        'username' => [
            'required' => 'اسم المستخدم مطلوب',
            'unique' => 'اسم المستخدم موجود مسبقا',
        ],
        'password' => [
            'required' => 'كلمة المرور مطلوبة',
            'min' => 'كلمة المرور لا تقل عن 6 رموز',
            'confirmed' => 'كلمة المرور غير متطابقة',
        ],
        'email' => [
            'required' => 'يجب ملء حقل البريد الإلكتروني',
            'email' => 'البريد الالكتروني خطأ',
            'max' => 'البريد الالكتروني لا يزيد عن 255 حرف',
            'unique' => 'البريد الالكتروني موجود مسبقا',
        ],
        'terms' => [
            'required' => 'يرجى الموافقة على الشروط والأحكام',
        ],

    ],
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [
        'city_id' => 'المدينه',
        'ar.name' => 'الاسم العربى',
        'en.name' => 'الاسم الانجليزى',
        'ar.nick_name' => 'اسم الكنية بالعربى',
        'en.nick_name' => 'اسم الكنية بالانجليزى',
        'branch_id' => 'رقم الفرع',
        'date_from'                     => 'بداية التاريخ',
        'date_to'                       => 'نهاية التاريخ',
        'ar'                            => 'الاسم بالعربى',
        'en'                            => 'الاسم بالانجليزى',
        'country_id'                    => 'الدولة',
        'time_from'                     => 'وقت البداية',
        'time_to'                       => 'وقت الانتهاء',
        'value'                         => 'القيمة',
        'rate'                          => 'النسبة',
        'type'                          => 'النوع',
        'category_id'                   => 'القسم',
        'notes'                         => 'ملاحظات',
        'nationality'                   => 'الجنسية',


    ],
        // my validation
];
