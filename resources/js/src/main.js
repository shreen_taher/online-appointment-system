/*=========================================================================================
  File Name: main.js
  Description: main vue(js) file
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import App from './App.vue'


// debugger
console.log('main trace:', store.state.currentLocale, store.state.authUser);

window.Vue = Vue;
// Vuesax Component Framework
import Vuesax from 'vuesax'

Vue.use(Vuesax)


// Vue Router
import router from './router'


// Vuex Store
import store from './store/store'

// axios
import axios from "./axios.js"
Vue.prototype.$http = axios

// import setupAxiosConfig from "./axios.js";

// setupAxiosConfig();

window.axios = axios;

// Theme Configurations
import '../themeConfig.js'

// Globally Registered Components
import './globalComponents.js'

// Vuejs - Vue wrapper for hammerjs
import { VueHammer } from 'vue2-hammer'
Vue.use(VueHammer)

// PrismJS
import 'prismjs'
import 'prismjs/themes/prism-tomorrow.css'


// Google Maps
import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
    load: {
        // Add your API key here
        key: 'AIzaSyB4DDathvvwuwlwnUu7F4Sow3oU22y5T1Y',
        libraries: 'places', // This is required if you use the Auto complete plug-in
    },
})

// VeeValidate
import VeeValidate, { Validator } from 'vee-validate';
Vue.use(VeeValidate);

import ar from 'vee-validate/dist/locale/ar';
import en from 'vee-validate/dist/locale/en';

import i18n from './localization/i18n'

window.i18n = i18n;

import { LanguageControl } from './services/AppLanguageService'

window.LanguageControl = LanguageControl;

import './mixins'

import moment from 'moment';
import { Form, HasError, AlertError } from 'vform'

window.Form = Form;
window.moment = moment;

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

// import VueI18n from 'vue-i18n'

// Vue.use(VueI18n)

import en_attributes from './validation-attributes-translation/en_attributes'
import ar_attributes from './validation-attributes-translation/ar_attributes'

Vue.filter('capitalize', function(text) {
    return text.charAt(0).toUpperCase() + text.slice(1)
})

Vue.filter('myDate', function(date) {
    return moment(date).format('MMMM Do YYYY'); // December 21st 2019
    // return date.moment().format('MMMM Do YYYY, h:mm:ss a'); // December 21st 2019, 11:32:52 am
})

// Vue select css
// Note: In latest version you have to add it separately
// import 'vue-select/dist/vue-select.css';

// import createPersistedState from "vuex-persistedstate";
// import { Store } from "vuex";

// const stored = new Store({
//   // ...
//   plugins: [createPersistedState({
//     paths: ['authUser', 'isAuthenticated']
//     })]
// });

Vue.config.productionTip = false

window.Fire = new Vue();

new Vue({
    router,
    store,
    i18n,
    beforeCreate() {
        store.dispatch("getLocales");
        LanguageControl.reloadCashedLanguage();
        console.log('locale:', store.state.currentLocale);

    },
    render: h => h(App)
}).$mount('#app')

const dictionary = {
    en: {
        messages: en.messages,
        attributes: en_attributes
    },
    ar: {
        messages: ar.messages,
        attributes: ar_attributes
    }
}
window.dictionary = dictionary;

Vue.use(
    VeeValidate, {
        locale: store.state.currentLocale,
        dictionary
    }
)
// https://forum.vuejs.org/t/persist-state-when-refreshing-the-browser/21505/2
// https://github.com/robinvdvleuten/vuex-persistedstate
// https://github.com/nuxt-community/axios-module/issues/171

// https://laracasts.com/discuss/channels/vue/vuejs-file-upload-not-working
// https://serversideup.net/uploading-files-vuejs-axios/

// icons: https://vue-feather-icons.netlify.com/

// https://docs.google.com/document/d/1JtP81INSGMHZMeNom5IqgsIQikYVqTw8bqmAx_KZWY8/edit?pli=1

// https://alligator.io/vuejs/vuex-persist-state

/*
    https://jsfiddle.net/
    https://codepen.io/
    https://jsbin.com/?html,output
    https://codesandbox.io/s/vue
*/
