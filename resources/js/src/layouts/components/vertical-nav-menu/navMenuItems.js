/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import i18n from '../../../localization/i18n'
import store from '../../../store/store'

let authUser = store.state.authUser,
    data = [];
if(authUser && authUser.user.role == 1)
    data = [
        {
        url: "/",
        name: "Home",
        slug: "home",
        icon: "HomeIcon",
        },
        {
        url: "/hosts",
        name: "hosts",
        slug: "hosts",
        icon: "GridIcon",
        },
    ];
else
    data = [
        {
        url: "/",
        name: "Home",
        slug: "home",
        icon: "HomeIcon",
        },
    ];
export default data
