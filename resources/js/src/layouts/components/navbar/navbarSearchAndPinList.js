export default {
  pages: {
    key: "title",
    data: [
      {title: 'Home',   url: '/',      icon: 'HomeIcon', is_bookmarked: false},
      {title: 'Page 2', url: '/page2', icon: 'FileIcon', is_bookmarked: false},
      {title: 'Categories', url: '/categories', icon: 'FileIcon', is_bookmarked: false},
      {title: 'SubCategories', url: '/subcategories', icon: 'FileIcon', is_bookmarked: false},
      {title: 'Groups', url: '/groups', icon: 'FileIcon', is_bookmarked: false},
      {title: 'producerCompanies', url: '/producer-companies', icon: 'FileIcon', is_bookmarked: false},
      {title: 'distributedCompanies', url: '/distributed-companies', icon: 'FileIcon', is_bookmarked: false},
      {title: 'branches', url: '/branches', icon: 'FileIcon', is_bookmarked: false},
      {title: 'clients', url: '/clients', icon: 'FileIcon', is_bookmarked: false},
      {title: 'products', url: '/products', icon: 'FileIcon', is_bookmarked: false},
      {title: 'models', url: '/models', icon: 'FileIcon', is_bookmarked: false},
      {title: 'experiments', url: '/experiments', icon: 'FileIcon', is_bookmarked: false},
      {title: 'reports', url: '/reports/single', icon: 'FileIcon', is_bookmarked: false},
      {title: 'product-movements', url: '/product-movements', icon: 'FileIcon', is_bookmarked: false},

      {title: 'table', url: '/table', icon: 'FileIcon', is_bookmarked: false},

    ]
  }
}
