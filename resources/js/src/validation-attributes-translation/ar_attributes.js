export default {
    title: "العنوان",
    main_image: "الصورة",
    name: "الاسم",
    "en.name": "الاسم بالانجليزى",
    "ar.name": "الاسم بالعربى",
    "en.description": "الوصف بالانجليزى",
    "ar.description": "الوصف بالعربى",
    "en.result": "النتيجة بالانجليزى",
    "ar.result": "النتيجة بالعربى",
    code: 'الكود',
    parent_id: "القسم الرئيسى",
    category_id: "القسم الرئيسى",
    subcategory_id: "القسم الفرعى",
    nationality: "الجنسية",
    address: "العنوان",
    city: "المدينه",
    country_id: "الدولة",
    order: "الترتيب",
    branch_id: "اسم الفرع",
    phone: "رقم الجوال",
    responsible_person: "الشخص المسؤل",
    group_id: "المجموعه",
    producer_company_id: "الشركة المنتجه",
    distributed_company_id: "الشركة الموزعه",
    yasin_code: "كود الياسين",
    company_code: "كود الشركة",
    unit: "الوحدة",
    status: "الحالة",
    description: "الوصف",
    "elements.en.name": "الاسم بالانجليزى",
    "elements.ar.name": "الاسم بالعربى",
    type: "النوع",
    "elements.element_type_id": "النوع",
    "element_0_en_name": "الاسم بالانجليزى",
    "element_0_ar_name": "الاسم بالعربى",
    "element_0_type": "النوع",
    "element_1_en_name": "الاسم بالانجليزى",
    "element_1_ar_name": "الاسم بالعربى",
    "element_1_type": "النوع",
    "element_2_en_name": "الاسم بالانجليزى",
    "element_2_ar_name": "الاسم بالعربى",
    "element_2_type": "النوع",

    start_date: "تاريخ بدء التجربه",
    end_date: "التاريخ المتوقع لانهاء التجربه",
    purpose: "الغرض من التجربه",
    area: "مساحة التجربه",
    weight: "وزن العبوة",
    weight_unit: "وحدة العبوة",
    model_id: "النموذج",
    client_id: "العميل",
    agriculture_type_id: "نوع الزراعه",
    area: "المساحه",
    quantity: "كمية الصنف",
    experimentCategories: "أصناف التجربة",
    comparisonCategories: "أصناف المقارن",
    witnessCategories: "الشاهد",
    production: "الانتاج",
    to_branch_id: "الفرع المنقول له",
    to_branch_id: "الفرع المنقول منه",
    email: "البريد الالكترونى او اسم المستخدم",
    password: "كلمة المرور",
    "en.city": "المدينة بالانجليزى",
    "ar.city": "المدينة بالعربى",
    "en.address": "العنوان بالانجليزى",
    "ar.address": "العنوان بالعربى",
    "en.nationality": "الجنسية بالانجليزى",
    "ar.nationality": "الجنسية بالعربى",
    productUnit: "العبوة الاصلية",
    orderbranch: "ترتيب الفرع",
    "en.productName": "المواصفات بالانجليزى",
    "ar.productName": "المواصفات بالعربى",
    "en.productDescription": "وصف الصنف بالانجليزى",
    "ar.productDescription": "وصف الصنف بالعربى",
    modelCode: "كود النموذج",
    reports_num: "عدد الزيارات",
    codeExperiment: "كود التجربة",
    experimentArea: "المساحة الاجمالية",
    areaUnit: "م2",
    expDescription: "ملاحظات التجربة",
}

