/*=========================================================================================
  File Name: router.js
  Description: Routes for vue-router. Lazy loading is enabled.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import Router from 'vue-router'
// Vuex Store
import store from './store/store'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior () {
        return { x: 0, y: 0 }
    },
    routes: [
        {
            path: '/login',
            name: 'login',
            meta: {
                noAuthentication: true
            },
            component: () => import('./views/pages/Login.vue')
        },
        {
            path: '/register',
            name: 'register',
            meta: {
                noAuthentication: true
            },
            component: () => import('@/views/pages/Register.vue')
          },

        {
    // =============================================================================
    // MAIN LAYOUT ROUTES
    // =============================================================================
            path: '',
            component: () => import('./layouts/main/Main.vue'),
            children: [
        // =============================================================================
        // Theme Routes
        // =============================================================================
              {
                path: '/',
                name: 'home',
                component: () => import('./views/Home.vue')
              },
            //   {
            //     path: '/page2',
            //     name: 'page-2',
            //     component: () => import('./views/Page2.vue')
            //   },
              {
                path: '/hosts',
                name: 'hosts',
                component: () => import('./views/pages/hosts/list/index.vue')
              },
              {
                path: '/appointments',
                name: 'appointments',
                props: true,
                component: () => import('./views/pages/appointments/list/index.vue')
              },

            ],
        },
    // =============================================================================
    // FULL PAGE LAYOUTS
    // =============================================================================
        {
            path: '',
            component: () => import('@/layouts/full-page/FullPage.vue'),
            children: [
        // =============================================================================
        // PAGES
        // =============================================================================

              {
                path: '/pages/error-404',
                name: 'page-error-404',
                component: () => import('@/views/pages/Error404.vue')
              },
            ]
        },
        // Redirect to 404 page, if no match found
        {
            path: '*',
            redirect: '/pages/error-404'
        }
    ],
})

// router.afterEach(() => {
//   // Remove initial loading
//   const appLoading = document.getElementById('loading-bg')
//     if (appLoading) {
//         appLoading.style.display = "none";
//     }
// })

// global navigation guard to validate authentication on any route
router.beforeEach((to, from, next) => {

    // console.log('is authenticated',
    // to, from, next,
    // store.state.isAuthenticated, to.meta.noAuthentication);
    if (to.meta.noAuthentication) {
        next();
    } else if(!to.name) {
        next({name: "page-error-404"});
    } else {
        if (store.state.isAuthenticated) {
            let authUser = store.state.authUser;
            if(to.name == 'hosts'){
                if(authUser && authUser.user.role == 1)
                    next()
                else
                    next({name: "page-error-404"});
            } else
                next()
        }
        //not auth and we redirect to login page
        else {
            next({name: "login"})
        }
    }

})

export default router
