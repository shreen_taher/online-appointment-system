import i18n from '@/localization/i18n'
import store from '../store/store';


export class LanguageControl {

    constructor() {}


    // get current app langauge
    static get cachedLanguage() {
        return localStorage.getItem('cachedLanguage');
    }
    // static set changeDirection(locale) {
        // let bootstrapRtl = document.querySelector('#bootstrap-rtl');
        // let rootEl = document.querySelector('html');
        // switch (locale) {
        //     case 'ar': {
        //         rootEl.classList.add('rtl');
        //         // bootstrapRtl.disabled = false;
        //         break;
        //     }
        //     case 'en': {
        //         rootEl.classList.remove('rtl');
        //         // bootstrapRtl.disabled = true;
        //         break;
        //     }
        //     default: {
        //         rootEl.classList.remove('rtl');
        //         // bootstrapRtl.disabled = true;
        //         console.log('nothing in storage, fallback to english');
        //         break;
        //     }
        // }
    // }


    // set new selected language
    static setSelectedLangauge(selectedLang) {
        // LanguageControl.changeDirection = selectedLang;
        localStorage.setItem('cachedLanguage', selectedLang);
        store.dispatch("changeLocale", {
            locale: selectedLang
        });
        i18n.locale = selectedLang;

    }


    // reloading previously selected langauge from local storage
    static reloadCashedLanguage() {
        let appLanguage = LanguageControl.cachedLanguage;
        console.log('app lang:', appLanguage);
        if (appLanguage) {
            LanguageControl.changeDirection = appLanguage;
            store.dispatch("changeLocale", {
                locale: appLanguage
            });
            i18n.locale = appLanguage;

        } else {
            LanguageControl.setSelectedLangauge('en');
            store.dispatch("changeLocale", {
                locale: 'en'
            });
            i18n.locale = 'en';

        }
        // console.log('reloadCashedLanguage method', store.state.currentLocale);

    } //
    //restet locale and direction on logout
    static onLogOut() {

    }
}
