import Vue from 'vue';

Vue.mixin({
    data() {
        return {

        }
    },
    methods: {
        uniqueID() {
            // Math.random should be unique because of its seeding algorithm.
            // Convert it to base 36 (numbers + letters), and grab the first 9 characters
            // after the decimal.
            return '_' + Math.random().toString(36).substr(2, 9);
        },
        cloneItem(obj) {
            //** deep clone */
            return JSON.parse(JSON.stringify(obj));
        },
        async handleBulkUploadFiles(files) {
            /*
            Iteate over any file sent over appending the files
            to the form data.
            */
            let formData = new FormData();

            for( var i = 0; i < files.length; i++ ){
                let file = files[i];
                formData.append('image[' + i + ']', file);
            }
            const config = {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            };
            let response = await axios.post("api/iPanel/image/bulkUpload", formData, config)
            return response;
        },
        async handleBulkDeleteFiles(attachments) {

            let response = await axios.post("api/iPanel/file/delete", {"attachments": attachments})
            return response;
        },

    }
})
