import axios from "axios"

const state = {
    currentLocale: "",
    locales: ["en", "ar"],
};

const getters = {
    currentLocale(state) {
        return state.currentLocale;
    },
};

const actions = {
    changeLocale(context, payload) {
        context.commit('setCurrentLocale', payload)
    },
    getLocales({commit}) {
        // axios.get("api/iPanel/locales")
        //     .then(res => {
        //         commit("setLocales", res);
        //     })
        //     .catch();
    },

};

const mutations = {

    setCurrentLocale(state, payload) {
        state.currentLocale = payload.locale;

    },
    setLocales(state, payload) {
        state.locales = payload;
    },

};


export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions
}
