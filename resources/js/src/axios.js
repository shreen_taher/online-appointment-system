// axios
import axios from 'axios'
import store from './store/store'

// const domain = ""
// console.log('start', store.state.authUser, store.state.currentLocale);

// debugger
// console.log('axios trace:', store.state.currentLocale, store.state.authUser);


export default axios.create({
//   domain,
  // You can add your headers here
  baseURL: 'http://localhost:7000',
  headers: getHeaders()
})
// window.axios.defaults.baseURL =  window.App.baseurl;

// export const HTTP = axios.create({
//     baseURL: 'https://maddox.auth.eu-west-1.amazoncognito.com/oauth2/',
//     headers: {
//       'Content-Type': 'application/x-www-form-urlencoded'
//     }
//   })
function getHeaders() {
    const authUser = store.state.authUser;
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Content-Language': store.state.currentLocale
    }
    // debugger;
    console.log('get headers', authUser, store.state.currentLocale);
    if (authUser) {
        headers['Authorization'] = `${authUser.token_type} ${authUser.access_token}`;
    }
    console.log('headers', headers);
    return headers;
}

// export default function setupAxiosConfig() {

//     axios.defaults.baseURL = 'http://localhost:8000'

//     //** to handle loading of requests */
//     axios.interceptors.request.use((config) => {
//             // set Headers
//             config.headers = getHeaders();
//             this.$vs.loading();
//             return config;
//         },
//         (error) => {
//             this.$vs.loading.close();
//             return Promise.reject(error);
//         });

//     axios.interceptors.response.use(
//         (response) => {
//             this.$vs.loading.close();
//             return response.data
//         },
//         (error) => {
//             let data = error.response.data;
//             this.$vs.loading.close();
//             return Promise.reject(data);
//         });
// }
