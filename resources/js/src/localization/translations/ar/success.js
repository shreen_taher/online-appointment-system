export default {
    loggedSuccessfully: "تم تسجيل الدخول بنجاح",
    itemCreated: "تم انشاء العنصر بنجاح",
    itemUpdated: "تم تعديل العنصر بنجاح",
    itemDeleted: "تم حذف العنصر بنجاح",
    success: "نجح",
}
