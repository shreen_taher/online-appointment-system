export default {
    producerName: "الشركة المنتجه",
    code: "الكود",
    nationality: "الجنسية",
    address: "العنوان",
    city: "المدينة",
    country_id: "الدولة",
    distributedName: "الشركة الموزعه",
}
