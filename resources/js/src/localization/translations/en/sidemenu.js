export default {
    categories:"categories",
    subcategories: "subcategories",
    groups: "groups",
    producerCompanies: "producers",
    distributedCompanies: "distributers",
    branches: "branches",
    clients: "clients",
    products: "products",
    models: "models",
    experiments: "experiments",
    reports: "reports",
    productMovements: "product movements",
    productMovement: "product movements",
    createMovement: "create movement",
    transferMovement: "transfer movement",
    deleteMovement: "delete movement",
    movements: "Movement Types",
}
