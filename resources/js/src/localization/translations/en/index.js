import common from './common'
import category from './category'
import company from './company'
import errors from './errors'
import success from './success'
import sidemenu from './sidemenu'

export default {
    common,
    category,
    company,
    errors,
    sidemenu,
    success
}
