export default {
    loggedSuccessfully: "User Logged in Successfully",
    itemCreated: "Item Created Successfully",
    itemUpdated: "Item Updated Successfully",
    itemDeleted: "Item Deleted Successfully",
    loadData: "Data loaded Successfully!",
    success: "success",
}
