export default {
    producerName: "Name",
    code: "Code",
    nationality: "Nationality",
    address: "Address",
    city: "City",
    country_id: "Country",
    distributedName: "Name",
}
