import Vue from 'vue';
import VueI18n from 'vue-i18n';
import translations from './translations'
import store from '../store/store'

Vue.use(VueI18n);
// const messages = {
//     'en': {welcomeMsg: 'Welcome to Your Vue.js App'},
//     'es': {welcomeMsg: 'Bienvenido a tu aplicación Vue.js'}
// };
const i18n = new VueI18n({
    locale: store.state.currentLocale, // set locale
    fallbackLocale: store.state.currentLocale, // set fallback locale
    messages: translations // set locale messages
});
export default i18n;
