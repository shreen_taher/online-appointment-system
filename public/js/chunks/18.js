(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[18],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/attachments.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/experiments/attachments.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import axios from 'axios'
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['item'],
  data: function data() {
    return {
      attachments: [],
      fileValid: false,
      experimentCategories: [],
      currentLocale: this.$store.state.currentLocale,
      form: new Form({
        id: '',
        user_id: '',
        ar: {
          note: ""
        },
        en: {
          note: ""
        },
        experiment_category_id: '',
        element_id: '',
        type: 'attachments',
        attachments: []
      })
    };
  },
  created: function created() {},
  mounted: function mounted() {
    this.loadData();
  },
  computed: {
    errors: {
      get: function get() {
        return this.errors;
      },
      set: function set(error) {
        return error;
      }
    }
  },
  methods: {
    loadData: function loadData() {
      var _this = this;

      Promise.all([this.getExperimentCategories()]).then(function (res) {
        _this.experimentCategories = res[0].response;
      });
    },
    getExperimentCategories: function () {
      var _getExperimentCategories = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return axios.get("api/iPanel/experiments/".concat(this.item.id, "/categories"));

              case 2:
                response = _context.sent;
                return _context.abrupt("return", response);

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getExperimentCategories() {
        return _getExperimentCategories.apply(this, arguments);
      }

      return getExperimentCategories;
    }(),
    cancelAction: function cancelAction() {
      if (this.attachments.length) {
        Promise.all([this.handleBulkDeleteFiles(this.attachments)]).then(function (res) {
          return true;
        });
      }

      this.navigateRoute();
    },
    handleUpload: function handleUpload(event) {
      var _this2 = this;

      var files = event.target.files;
      var requests = [this.handleBulkUploadFiles(files)];

      if (this.attachments.length) {
        requests.push(this.handleBulkDeleteFiles(this.attachments));
      }

      Promise.all(requests).then(function (res) {
        _this2.form.attachments = _this2.attachments = res[0].response;
        _this2.fileValid = _this2.$t('common.FileUploadedSuccessfully');
      });
    },
    submitForm: function submitForm() {
      var _this3 = this;

      this.$validator.validateAll().then(function (result) {
        if (result) {
          _this3.experimentAttachments();
        } else {// form have errors
        }
      });
    },
    navigateRoute: function navigateRoute() {
      this.$router.push({
        name: 'experiments'
      });
    },
    experimentAttachments: function experimentAttachments() {
      var _this4 = this;

      axios.post("api/iPanel/experiments/".concat(this.item.id, "/attachments"), this.form).then(function (data) {
        _this4.$vs.notify({
          color: 'success',
          title: 'success',
          text: _this4.$t('success.resultCreated'),
          position: 'top-right'
        });

        _this4.navigateRoute();
      }).catch(function (error) {
        //** A simple way to handle Laravel back-end validation in Vue  */
        _this4.form['errors']['errors'] = error.errors;
      });
    },
    resetForm: function resetForm() {
      this.form.clear(); //** Clear the form errors. */

      this.form.reset(); //** Reset the form fields. */
    }
  },
  watch: {}
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/attachments.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/experiments/attachments.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".vs-switch{\n  width: 80px !important;\n}\n.vs-switch--text {\n  font-size: 1rem !important;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/attachments.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/experiments/attachments.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./attachments.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/attachments.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/attachments.vue?vue&type=template&id=2405d55c&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/experiments/attachments.vue?vue&type=template&id=2405d55c& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "page-user-edit" } },
    [
      _c("vx-card", { attrs: { title: _vm.$t("common.attachments") } }, [
        _c(
          "div",
          {
            staticClass: "tabs-container px-6 pt-6",
            attrs: { slot: "no-body" },
            slot: "no-body"
          },
          [
            _c(
              "form",
              [
                _c("div", { staticClass: "vx-row" }, [
                  _c(
                    "div",
                    { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                    [
                      _c(
                        "vs-select",
                        {
                          staticClass: "selectExample mt-5 w-full",
                          attrs: {
                            autocomplete: "",
                            name: "experiment_category_id"
                          },
                          model: {
                            value: _vm.form.experiment_category_id,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "experiment_category_id", $$v)
                            },
                            expression: "form.experiment_category_id"
                          }
                        },
                        [
                          _c("vs-select-item", {
                            attrs: {
                              value: "",
                              text: _vm.$t("common.experiment_category_id")
                            }
                          }),
                          _vm._v(" "),
                          _vm._l(_vm.experimentCategories, function(
                            item,
                            index
                          ) {
                            return _c("vs-select-item", {
                              key: index,
                              attrs: {
                                value: item.id,
                                text: item[_vm.currentLocale].name
                              }
                            })
                          })
                        ],
                        2
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: _vm.errors.has("experiment_category_id"),
                              expression: "errors.has('experiment_category_id')"
                            }
                          ],
                          staticClass: "text-danger text-sm"
                        },
                        [
                          _vm._v(
                            _vm._s(_vm.errors.first("experiment_category_id"))
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("has-error", {
                        staticClass: "text-danger text-sm",
                        attrs: {
                          form: _vm.form,
                          field: "experiment_category_id"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                    [
                      _c(
                        "vs-select",
                        {
                          staticClass: "selectExample mt-5 w-full",
                          attrs: { autocomplete: "", name: "element_id" },
                          model: {
                            value: _vm.form.element_id,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "element_id", $$v)
                            },
                            expression: "form.element_id"
                          }
                        },
                        [
                          _c("vs-select-item", {
                            attrs: {
                              value: "",
                              text: _vm.$t("common.element_id")
                            }
                          }),
                          _vm._v(" "),
                          _vm._l(_vm.item.elements, function(item, index) {
                            return _c("vs-select-item", {
                              key: index,
                              attrs: {
                                value: item.id,
                                text: item[_vm.currentLocale].name
                              }
                            })
                          })
                        ],
                        2
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: _vm.errors.has("element_id"),
                              expression: "errors.has('element_id')"
                            }
                          ],
                          staticClass: "text-danger text-sm"
                        },
                        [_vm._v(_vm._s(_vm.errors.first("element_id")))]
                      ),
                      _vm._v(" "),
                      _c("has-error", {
                        staticClass: "text-danger text-sm",
                        attrs: { form: _vm.form, field: "element_id" }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "vx-row" }, [
                  _c(
                    "div",
                    { staticClass: "vx-col w-full mb-2" },
                    [
                      _c("vs-textarea", {
                        staticClass: "mt-5 w-full",
                        attrs: {
                          label: _vm.$t("common['en.note']"),
                          name: "en.note"
                        },
                        model: {
                          value: _vm.form.en.note,
                          callback: function($$v) {
                            _vm.$set(_vm.form.en, "note", $$v)
                          },
                          expression: "form.en.note"
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "span",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: _vm.errors.has("en.note"),
                              expression: "errors.has('en.note')"
                            }
                          ],
                          staticClass: "text-danger text-sm"
                        },
                        [_vm._v(_vm._s(_vm.errors.first("en.note")))]
                      ),
                      _vm._v(" "),
                      _c("has-error", {
                        staticClass: "text-danger text-sm",
                        attrs: { form: _vm.form, field: "en.note" }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "vx-col w-full mb-2" },
                    [
                      _c("vs-textarea", {
                        staticClass: "mt-5 w-full",
                        attrs: {
                          label: _vm.$t("common['ar.note']"),
                          name: "ar.note"
                        },
                        model: {
                          value: _vm.form.ar.note,
                          callback: function($$v) {
                            _vm.$set(_vm.form.ar, "note", $$v)
                          },
                          expression: "form.ar.note"
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "span",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: _vm.errors.has("ar.note"),
                              expression: "errors.has('ar.note')"
                            }
                          ],
                          staticClass: "text-danger text-sm"
                        },
                        [_vm._v(_vm._s(_vm.errors.first("ar.note")))]
                      ),
                      _vm._v(" "),
                      _c("has-error", {
                        staticClass: "text-danger text-sm",
                        attrs: { form: _vm.form, field: "ar.note" }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "vx-row" }, [
                  _c(
                    "div",
                    { staticClass: "vx-col w-full mb-2" },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "validate",
                            rawName: "v-validate",
                            value: "required",
                            expression: "'required'"
                          }
                        ],
                        ref: "inputFiles",
                        staticStyle: { display: "none" },
                        attrs: {
                          type: "file",
                          multiple: "",
                          name: "attachments"
                        },
                        on: { change: _vm.handleUpload }
                      }),
                      _vm._v(" "),
                      _c(
                        "vs-button",
                        {
                          attrs: {
                            color: "success",
                            type: "filled",
                            "icon-pack": "feather",
                            icon: "icon-plus"
                          },
                          on: {
                            click: function($event) {
                              return _vm.$refs.inputFiles.click()
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n                          " +
                              _vm._s(_vm.$t("common.upload")) +
                              "\n                      "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: _vm.errors.has("attachments"),
                              expression: "errors.has('attachments')"
                            }
                          ],
                          staticClass: "text-danger text-sm"
                        },
                        [_vm._v(_vm._s(_vm.errors.first("attachments")))]
                      ),
                      _vm._v(" "),
                      _c("has-error", {
                        staticClass: "text-danger text-sm",
                        attrs: { form: _vm.form, field: "attachments" }
                      }),
                      _vm._v(" "),
                      _vm.fileValid
                        ? _c(
                            "span",
                            {
                              staticClass: "mt-4",
                              staticStyle: { color: "#33691e" }
                            },
                            [_vm._v(_vm._s(_vm.fileValid))]
                          )
                        : _vm._e()
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("vs-divider"),
                _vm._v(" "),
                _c("div", { staticClass: "vx-row" }, [
                  _c(
                    "div",
                    { staticClass: "vx-col sm:w-2/3 w-full ml-auto" },
                    [
                      _c(
                        "vs-button",
                        {
                          staticClass: "mr-3 mb-2",
                          attrs: { type: "filled" },
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              return _vm.submitForm($event)
                            }
                          }
                        },
                        [_vm._v(_vm._s(_vm.$t("common.submit")))]
                      ),
                      _vm._v(" "),
                      _c(
                        "vs-button",
                        {
                          staticClass: "mb-2",
                          attrs: { color: "warning", type: "border" },
                          on: { click: _vm.cancelAction }
                        },
                        [_vm._v(_vm._s(_vm.$t("common.cancel")))]
                      )
                    ],
                    1
                  )
                ])
              ],
              1
            )
          ]
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/pages/experiments/attachments.vue":
/*!******************************************************************!*\
  !*** ./resources/js/src/views/pages/experiments/attachments.vue ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _attachments_vue_vue_type_template_id_2405d55c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./attachments.vue?vue&type=template&id=2405d55c& */ "./resources/js/src/views/pages/experiments/attachments.vue?vue&type=template&id=2405d55c&");
/* harmony import */ var _attachments_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./attachments.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/experiments/attachments.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _attachments_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./attachments.vue?vue&type=style&index=0&lang=css& */ "./resources/js/src/views/pages/experiments/attachments.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _attachments_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _attachments_vue_vue_type_template_id_2405d55c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _attachments_vue_vue_type_template_id_2405d55c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/experiments/attachments.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/experiments/attachments.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/src/views/pages/experiments/attachments.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_attachments_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./attachments.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/attachments.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_attachments_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/experiments/attachments.vue?vue&type=style&index=0&lang=css&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/experiments/attachments.vue?vue&type=style&index=0&lang=css& ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_attachments_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./attachments.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/attachments.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_attachments_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_attachments_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_attachments_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_attachments_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_attachments_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/pages/experiments/attachments.vue?vue&type=template&id=2405d55c&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/experiments/attachments.vue?vue&type=template&id=2405d55c& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_attachments_vue_vue_type_template_id_2405d55c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./attachments.vue?vue&type=template&id=2405d55c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/attachments.vue?vue&type=template&id=2405d55c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_attachments_vue_vue_type_template_id_2405d55c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_attachments_vue_vue_type_template_id_2405d55c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);