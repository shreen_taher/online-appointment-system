(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[22],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/models/single.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/models/single.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['editMode', 'item', 'categories', 'elementTypes'],
  data: function data() {
    return {
      subcategories: [],
      groups: [],
      products: [],
      currentLocale: this.$store.state.currentLocale,
      form: Object.assign({}, this.item)
    };
  },
  created: function created() {},
  mounted: function mounted() {
    this.loadAllData(); //   this.form = Object.assign({}, this.item);

    console.log('form>>>', this.form);
  },
  computed: {
    errors: {
      get: function get() {
        return this.errors;
      },
      set: function set(error) {
        return error;
      }
    }
  },
  methods: {
    uploadAttachment: function uploadAttachment(event) {
      console.log('upload', event.target.files);
      this.attachments = event.target.files;
    },
    submitForm: function submitForm() {
      var _this = this;

      this.$validator.validateAll().then(function (result) {
        if (result) {
          if (_this.editMode) _this.updateItem();else _this.createItem();
        } else {// form have errors
        }
      });
    },
    checkSelectType: function checkSelectType(value, key) {
      console.log(value, key);
      if (value == 5) // select
        this.addElementOption(key);else // reset options
        this.form.elements[key].options = [];
    },
    addElement: function addElement() {
      this.form.elements.push({
        ar: {
          name: ""
        },
        en: {
          name: ""
        },
        element_type_id: '',
        options: [],
        status: true
      });
    },
    removeElement: function removeElement(key) {
      this.form.elements.splice(key, 1);
    },
    addElementOption: function addElementOption(key) {
      this.form.elements[key].options.push({
        name_en: "",
        name_ar: ""
      });
    },
    removeElementOption: function removeElementOption(key, index) {
      this.form.elements[key].options.splice(index, 1);
    },
    navigateRoute: function navigateRoute() {
      this.$router.push({
        name: 'models'
      });
    },
    createItem: function createItem() {
      var _this2 = this;

      axios.post('api/iPanel/models', this.form).then(function (data) {
        _this2.$vs.notify({
          color: 'success',
          title: 'success',
          text: _this2.$t('success.itemCreated'),
          position: 'top-right'
        });

        _this2.navigateRoute();
      }).catch(function (error) {
        //** A simple way to handle Laravel back-end validation in Vue  */
        _this2.form['errors']['errors'] = error.errors;
      });
    },
    updateItem: function updateItem() {
      var _this3 = this;

      axios.put("api/iPanel/models/".concat(this.form.id), this.form).then(function (data) {
        _this3.$vs.notify({
          color: 'success',
          title: 'success',
          text: _this3.$t('success.itemUpdated'),
          position: 'top-right'
        });

        _this3.navigateRoute();
      }).catch(function (error) {
        //** A simple way to handle Laravel back-end validation in Vue  */
        _this3.form['errors']['errors'] = error.errors;
      });
    },
    // initialData() {
    //     // this.resetForm();
    //     if(this.form && this.form.id) this.form.fill(this.form);
    // },
    resetForm: function resetForm() {
      this.form.clear(); //** Clear the form errors. */

      this.form.reset(); //** Reset the form fields. */
    },
    getCategories: function () {
      var _getCategories = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return axios.get('api/iPanel/categories');

              case 2:
                response = _context.sent;
                return _context.abrupt("return", response);

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function getCategories() {
        return _getCategories.apply(this, arguments);
      }

      return getCategories;
    }(),
    loadAllData: function loadAllData() {
      if (this.editMode) this.loadSelectData();
    },
    loadSelectData: function loadSelectData() {
      var _this4 = this;

      Promise.all([this.getSubCategories(), this.getGroups(), this.getProducts()]).then(function (res) {
        _this4.subcategories = res[0].response;
        _this4.groups = res[1].response;
        _this4.products = res[2].response;
      });
    },
    getSubCategories: function () {
      var _getSubCategories = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return axios.get("api/iPanel/subcategories/".concat(this.form.category_id));

              case 2:
                response = _context2.sent;
                return _context2.abrupt("return", response);

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function getSubCategories() {
        return _getSubCategories.apply(this, arguments);
      }

      return getSubCategories;
    }(),
    getGroups: function () {
      var _getGroups = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return axios.get("api/iPanel/groups/".concat(this.form.subcategory_id));

              case 2:
                response = _context3.sent;
                return _context3.abrupt("return", response);

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function getGroups() {
        return _getGroups.apply(this, arguments);
      }

      return getGroups;
    }(),
    getProducts: function () {
      var _getProducts = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return axios.get("api/iPanel/get-products/".concat(this.form.group_id));

              case 2:
                response = _context4.sent;
                return _context4.abrupt("return", response);

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function getProducts() {
        return _getProducts.apply(this, arguments);
      }

      return getProducts;
    }()
  },
  watch: {
    'form.category_id': function formCategory_id(val, oldVal) {
      var _this5 = this;

      //** reset old value */
      this.form.subcategory_id = '';
      this.form.group_id = '';
      this.form.product_id = '';
      if (this.form.category_id != '') Promise.all([this.getSubCategories()]).then(function (res) {
        _this5.subcategories = res[0].response;
      });else this.subcategories = [];
    },
    'form.subcategory_id': function formSubcategory_id(val, oldVal) {
      var _this6 = this;

      //** reset old value */
      this.form.group_id = '';
      this.form.product_id = '';
      if (this.form.subcategory_id != '') Promise.all([this.getGroups()]).then(function (res) {
        _this6.groups = res[0].response;
      });else this.groups = [];
    },
    'form.group_id': function formGroup_id(val, oldVal) {
      var _this7 = this;

      //** reset old value */
      this.form.product_id = '';
      if (this.form.group_id != '') Promise.all([this.getProducts()]).then(function (res) {
        _this7.products = res[0].response;
      });else this.products = [];
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/models/single.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/models/single.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".vs-switch{\n  width: 80px !important;\n}\n.vs-switch--text {\n  font-size: 1rem !important;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/models/single.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/models/single.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/models/single.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/models/single.vue?vue&type=template&id=c5457330&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/models/single.vue?vue&type=template&id=c5457330& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "page-user-edit" } },
    [
      _c(
        "vx-card",
        {
          attrs: {
            title: !_vm.editMode
              ? _vm.$t("common.create")
              : _vm.$t("common.edit")
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "tabs-container px-6 pt-6",
              attrs: { slot: "no-body" },
              slot: "no-body"
            },
            [
              _c(
                "form",
                [
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required",
                              expression: "'required'"
                            }
                          ],
                          staticClass: "w-full",
                          attrs: {
                            "label-placeholder": _vm.$t("common['en.name']"),
                            name: "en.name"
                          },
                          model: {
                            value: _vm.form.en.name,
                            callback: function($$v) {
                              _vm.$set(_vm.form.en, "name", $$v)
                            },
                            expression: "form.en.name"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("en.name"),
                                expression: "errors.has('en.name')"
                              }
                            ],
                            staticClass: "text-danger text-sm",
                            attrs: { "data-vv-as": "en.name" }
                          },
                          [_vm._v(_vm._s(_vm.errors.first("en.name")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "en.name" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required",
                              expression: "'required'"
                            }
                          ],
                          staticClass: "w-full",
                          attrs: {
                            "label-placeholder": _vm.$t("common['ar.name']"),
                            name: "ar.name"
                          },
                          model: {
                            value: _vm.form.ar.name,
                            callback: function($$v) {
                              _vm.$set(_vm.form.ar, "name", $$v)
                            },
                            expression: "form.ar.name"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("ar.name"),
                                expression: "errors.has('ar.name')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("ar.name")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "ar.name" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col w-full mb-2" },
                      [
                        _c(
                          "vs-select",
                          {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "selectExample mt-5 w-full",
                            attrs: { autocomplete: "", name: "category_id" },
                            model: {
                              value: _vm.form.category_id,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "category_id", $$v)
                              },
                              expression: "form.category_id"
                            }
                          },
                          [
                            _c("vs-select-item", {
                              attrs: {
                                value: "",
                                text: _vm.$t("category.category_id")
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.categories, function(item, index) {
                              return _c("vs-select-item", {
                                key: index,
                                attrs: {
                                  value: item.id,
                                  text: item[_vm.currentLocale].name
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("category_id"),
                                expression: "errors.has('category_id')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("category_id")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "category_id" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c(
                          "vs-select",
                          {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "selectExample mt-5 w-full",
                            attrs: { autocomplete: "", name: "subcategory_id" },
                            model: {
                              value: _vm.form.subcategory_id,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "subcategory_id", $$v)
                              },
                              expression: "form.subcategory_id"
                            }
                          },
                          [
                            _c("vs-select-item", {
                              attrs: {
                                value: "",
                                text: _vm.$t("category.subcategory_id")
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.subcategories, function(item, index) {
                              return _c("vs-select-item", {
                                key: index,
                                attrs: {
                                  value: item.id,
                                  text: item[_vm.currentLocale].name
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("subcategory_id"),
                                expression: "errors.has('subcategory_id')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("subcategory_id")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "subcategory_id" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c(
                          "vs-select",
                          {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "selectExample mt-5 w-full",
                            attrs: { autocomplete: "", name: "group_id" },
                            model: {
                              value: _vm.form.group_id,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "group_id", $$v)
                              },
                              expression: "form.group_id"
                            }
                          },
                          [
                            _c("vs-select-item", {
                              attrs: {
                                value: "",
                                text: _vm.$t("category.group_id")
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.groups, function(item, index) {
                              return _c("vs-select-item", {
                                key: index,
                                attrs: {
                                  value: item.id,
                                  text: item[_vm.currentLocale].name
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("group_id"),
                                expression: "errors.has('group_id')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("group_id")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "group_id" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c(
                          "vs-select",
                          {
                            staticClass: "selectExample mt-5 w-full",
                            attrs: { autocomplete: "", name: "product_id" },
                            model: {
                              value: _vm.form.product_id,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "product_id", $$v)
                              },
                              expression: "form.product_id"
                            }
                          },
                          [
                            _c("vs-select-item", {
                              attrs: {
                                value: "",
                                text: _vm.$t("category.product_id")
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.products, function(item, index) {
                              return _c("vs-select-item", {
                                key: index,
                                attrs: {
                                  value: item.id,
                                  text: item[_vm.currentLocale].name
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("product_id"),
                                expression: "errors.has('product_id')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("product_id")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "product_id" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required",
                              expression: "'required'"
                            }
                          ],
                          staticClass: "mt-5 w-full",
                          attrs: {
                            "label-placeholder": _vm.$t("common.modelCode"),
                            name: "modelCode"
                          },
                          model: {
                            value: _vm.form.code,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "code", $$v)
                            },
                            expression: "form.code"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("modelCode"),
                                expression: "errors.has('modelCode')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("modelCode")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "code" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "vx-card",
                    { attrs: { title: _vm.$t("common.modelElements") } },
                    [
                      _vm._l(_vm.form.elements, function(element, key) {
                        return _c(
                          "div",
                          { key: "element" + key, staticClass: "px-3" },
                          [
                            _c("div", { staticClass: "vx-row" }, [
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c("vs-input", {
                                    directives: [
                                      {
                                        name: "validate",
                                        rawName: "v-validate",
                                        value: "required",
                                        expression: "'required'"
                                      }
                                    ],
                                    staticClass: "w-full",
                                    attrs: {
                                      "label-placeholder": _vm.$t(
                                        "common['elements.en.name']"
                                      ),
                                      name: "element_" + key + "_en_name"
                                    },
                                    model: {
                                      value: element.en.name,
                                      callback: function($$v) {
                                        _vm.$set(element.en, "name", $$v)
                                      },
                                      expression: "element.en.name"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has(
                                            "element_" + key + "_en_name"
                                          ),
                                          expression:
                                            "errors.has(`element_${key}_en_name`)"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm",
                                      attrs: { "data-vv-as": element.en.name }
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.errors.first(
                                            "element_" + key + "_en_name"
                                          )
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: {
                                      form: _vm.form,
                                      field: "elements." + key + ".en.name"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c("vs-input", {
                                    directives: [
                                      {
                                        name: "validate",
                                        rawName: "v-validate",
                                        value: "required",
                                        expression: "'required'"
                                      }
                                    ],
                                    staticClass: "w-full",
                                    attrs: {
                                      "label-placeholder": _vm.$t(
                                        "common['elements.ar.name']"
                                      ),
                                      name: "element_" + key + "_ar_name"
                                    },
                                    model: {
                                      value: element.ar.name,
                                      callback: function($$v) {
                                        _vm.$set(element.ar, "name", $$v)
                                      },
                                      expression: "element.ar.name"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has(
                                            "element_" + key + "_ar_name"
                                          ),
                                          expression:
                                            "errors.has(`element_${key}_ar_name`)"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm"
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.errors.first(
                                            "element_" + key + "_ar_name"
                                          )
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: {
                                      form: _vm.form,
                                      field: "elements." + key + ".ar.name"
                                    }
                                  })
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "vx-row" }, [
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c(
                                    "vs-select",
                                    {
                                      directives: [
                                        {
                                          name: "validate",
                                          rawName: "v-validate",
                                          value: "required",
                                          expression: "'required'"
                                        }
                                      ],
                                      staticClass: "selectExample mt-5 w-full",
                                      attrs: {
                                        autocomplete: "",
                                        name: "element_" + key + "_type"
                                      },
                                      on: {
                                        change: function($event) {
                                          return _vm.checkSelectType(
                                            $event,
                                            key
                                          )
                                        }
                                      },
                                      model: {
                                        value: element.element_type_id,
                                        callback: function($$v) {
                                          _vm.$set(
                                            element,
                                            "element_type_id",
                                            $$v
                                          )
                                        },
                                        expression: "element.element_type_id"
                                      }
                                    },
                                    [
                                      _c("vs-select-item", {
                                        attrs: {
                                          value: "",
                                          text: _vm.$t("common.type")
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm._l(_vm.elementTypes, function(
                                        item,
                                        index
                                      ) {
                                        return _c("vs-select-item", {
                                          key: index,
                                          attrs: {
                                            value: item.id,
                                            text: item[_vm.currentLocale].name
                                          }
                                        })
                                      })
                                    ],
                                    2
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has(
                                            "element_" + key + "_type"
                                          ),
                                          expression:
                                            "errors.has(`element_${key}_type`)"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm"
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.errors.first(
                                            "element_" + key + "_type"
                                          )
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: {
                                      form: _vm.form,
                                      field:
                                        "elements." + key + ".element_type_id"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "vx-col sm:w-1/2 w-full mb-2 mt-8"
                                },
                                [
                                  _c(
                                    "vs-switch",
                                    {
                                      attrs: { color: "success" },
                                      model: {
                                        value: element.status,
                                        callback: function($$v) {
                                          _vm.$set(element, "status", $$v)
                                        },
                                        expression: "element.status"
                                      }
                                    },
                                    [
                                      _c(
                                        "span",
                                        { attrs: { slot: "on" }, slot: "on" },
                                        [
                                          _vm._v(
                                            _vm._s(_vm.$t("common.required"))
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        { attrs: { slot: "off" }, slot: "off" },
                                        [
                                          _vm._v(
                                            _vm._s(_vm.$t("common.optional"))
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "vx-card",
                              {
                                directives: [
                                  {
                                    name: "show",
                                    rawName: "v-show",
                                    value: element.options.length,
                                    expression: "element.options.length"
                                  }
                                ],
                                attrs: {
                                  title: _vm.$t("common.modelElementOptions")
                                }
                              },
                              [
                                _vm._l(element.options, function(
                                  elementOption,
                                  index
                                ) {
                                  return _c(
                                    "div",
                                    {
                                      key: "element" + index,
                                      staticClass: "px-3"
                                    },
                                    [
                                      _c("div", { staticClass: "vx-row" }, [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "vx-col sm:w-1/2 w-full mb-2"
                                          },
                                          [
                                            _c("vs-input", {
                                              directives: [
                                                {
                                                  name: "validate",
                                                  rawName: "v-validate",
                                                  value: "required",
                                                  expression: "'required'"
                                                }
                                              ],
                                              staticClass: "w-full",
                                              attrs: {
                                                "label-placeholder": _vm.$t(
                                                  "common['options.en.name']"
                                                ),
                                                name:
                                                  "element_" +
                                                  key +
                                                  "_option_" +
                                                  index +
                                                  "_en_name"
                                              },
                                              model: {
                                                value: elementOption.name_en,
                                                callback: function($$v) {
                                                  _vm.$set(
                                                    elementOption,
                                                    "name_en",
                                                    $$v
                                                  )
                                                },
                                                expression:
                                                  "elementOption.name_en"
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "span",
                                              {
                                                directives: [
                                                  {
                                                    name: "show",
                                                    rawName: "v-show",
                                                    value: _vm.errors.has(
                                                      "element_" +
                                                        key +
                                                        "_option_" +
                                                        index +
                                                        "_en_name"
                                                    ),
                                                    expression:
                                                      "errors.has(`element_${key}_option_${index}_en_name`)"
                                                  }
                                                ],
                                                staticClass:
                                                  "text-danger text-sm"
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    _vm.errors.first(
                                                      "element_" +
                                                        key +
                                                        "_option_" +
                                                        index +
                                                        "_en_name"
                                                    )
                                                  )
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c("has-error", {
                                              staticClass:
                                                "text-danger text-sm",
                                              attrs: {
                                                form: _vm.form,
                                                field:
                                                  "elements." +
                                                  key +
                                                  ".options." +
                                                  index +
                                                  ".name_en"
                                              }
                                            })
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "vx-col sm:w-1/2 w-full mb-2"
                                          },
                                          [
                                            _c("vs-input", {
                                              directives: [
                                                {
                                                  name: "validate",
                                                  rawName: "v-validate",
                                                  value: "required",
                                                  expression: "'required'"
                                                }
                                              ],
                                              staticClass: "w-full",
                                              attrs: {
                                                "label-placeholder": _vm.$t(
                                                  "common['options.ar.name']"
                                                ),
                                                name:
                                                  "element_" +
                                                  key +
                                                  "_option_" +
                                                  index +
                                                  "_ar_name"
                                              },
                                              model: {
                                                value: elementOption.name_ar,
                                                callback: function($$v) {
                                                  _vm.$set(
                                                    elementOption,
                                                    "name_ar",
                                                    $$v
                                                  )
                                                },
                                                expression:
                                                  "elementOption.name_ar"
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "span",
                                              {
                                                directives: [
                                                  {
                                                    name: "show",
                                                    rawName: "v-show",
                                                    value: _vm.errors.has(
                                                      "element_" +
                                                        key +
                                                        "_option_" +
                                                        index +
                                                        "_ar_name"
                                                    ),
                                                    expression:
                                                      "errors.has(`element_${key}_option_${index}_ar_name`)"
                                                  }
                                                ],
                                                staticClass:
                                                  "text-danger text-sm"
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    _vm.errors.first(
                                                      "element_" +
                                                        key +
                                                        "_option_" +
                                                        index +
                                                        "_ar_name"
                                                    )
                                                  )
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c("has-error", {
                                              staticClass:
                                                "text-danger text-sm",
                                              attrs: {
                                                form: _vm.form,
                                                field:
                                                  "elements." +
                                                  key +
                                                  ".options." +
                                                  index +
                                                  ".name_ar"
                                              }
                                            })
                                          ],
                                          1
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("vs-button", {
                                        directives: [
                                          {
                                            name: "show",
                                            rawName: "v-show",
                                            value: index,
                                            expression: "index"
                                          }
                                        ],
                                        staticClass: "mb-4 md:mb-0",
                                        staticStyle: { margin: "auto" },
                                        attrs: {
                                          "icon-pack": "feather",
                                          type: "border",
                                          color: "danger",
                                          icon: "icon-trash"
                                        },
                                        on: {
                                          click: function($event) {
                                            return _vm.removeElementOption(
                                              key,
                                              index
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("vs-divider")
                                    ],
                                    1
                                  )
                                }),
                                _vm._v(" "),
                                _c("vs-button", {
                                  staticClass: "mb-4 md:mb-0",
                                  staticStyle: { margin: "auto" },
                                  attrs: {
                                    color: "primary",
                                    type: "filled",
                                    "icon-pack": "feather",
                                    icon: "icon-plus"
                                  },
                                  on: {
                                    click: function($event) {
                                      return _vm.addElementOption(key)
                                    }
                                  }
                                })
                              ],
                              2
                            ),
                            _vm._v(" "),
                            _c("vs-button", {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: key,
                                  expression: "key"
                                }
                              ],
                              staticClass: "mb-4 md:mb-0",
                              staticStyle: { margin: "auto" },
                              attrs: {
                                "icon-pack": "feather",
                                type: "border",
                                color: "danger",
                                icon: "icon-trash"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.removeElement(key)
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("vs-divider")
                          ],
                          1
                        )
                      }),
                      _vm._v(" "),
                      _c("vs-button", {
                        staticClass: "mb-4 md:mb-0",
                        staticStyle: { margin: "auto" },
                        attrs: {
                          color: "primary",
                          type: "filled",
                          "icon-pack": "feather",
                          icon: "icon-plus"
                        },
                        on: { click: _vm.addElement }
                      })
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col w-full mb-2 mt-4" },
                      [
                        _c(
                          "vs-switch",
                          {
                            attrs: { color: "success" },
                            model: {
                              value: _vm.form.is_active,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "is_active", $$v)
                              },
                              expression: "form.is_active"
                            }
                          },
                          [
                            _c("span", { attrs: { slot: "on" }, slot: "on" }, [
                              _vm._v(_vm._s(_vm.$t("common.open")))
                            ]),
                            _vm._v(" "),
                            _c(
                              "span",
                              { attrs: { slot: "off" }, slot: "off" },
                              [_vm._v(_vm._s(_vm.$t("common.close")))]
                            )
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("vs-divider"),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-2/3 w-full ml-auto" },
                      [
                        _c(
                          "vs-button",
                          {
                            staticClass: "mr-3 mb-2",
                            attrs: { type: "filled" },
                            on: {
                              click: function($event) {
                                $event.preventDefault()
                                return _vm.submitForm($event)
                              }
                            }
                          },
                          [_vm._v(_vm._s(_vm.$t("common.submit")))]
                        ),
                        _vm._v(" "),
                        _c(
                          "vs-button",
                          {
                            staticClass: "mb-2",
                            attrs: { color: "warning", type: "border" },
                            on: { click: _vm.navigateRoute }
                          },
                          [_vm._v(_vm._s(_vm.$t("common.cancel")))]
                        )
                      ],
                      1
                    )
                  ])
                ],
                1
              )
            ]
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/pages/models/single.vue":
/*!********************************************************!*\
  !*** ./resources/js/src/views/pages/models/single.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _single_vue_vue_type_template_id_c5457330___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./single.vue?vue&type=template&id=c5457330& */ "./resources/js/src/views/pages/models/single.vue?vue&type=template&id=c5457330&");
/* harmony import */ var _single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./single.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/models/single.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./single.vue?vue&type=style&index=0&lang=css& */ "./resources/js/src/views/pages/models/single.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _single_vue_vue_type_template_id_c5457330___WEBPACK_IMPORTED_MODULE_0__["render"],
  _single_vue_vue_type_template_id_c5457330___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/models/single.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/models/single.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/src/views/pages/models/single.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/models/single.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/models/single.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/src/views/pages/models/single.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/models/single.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/pages/models/single.vue?vue&type=template&id=c5457330&":
/*!***************************************************************************************!*\
  !*** ./resources/js/src/views/pages/models/single.vue?vue&type=template&id=c5457330& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_c5457330___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=template&id=c5457330& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/models/single.vue?vue&type=template&id=c5457330&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_c5457330___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_c5457330___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);