(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[20],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/single.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/experiments/single.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
/* harmony import */ var vuejs_datepicker_src_locale__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuejs-datepicker/src/locale */ "./node_modules/vuejs-datepicker/src/locale/index.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['editMode', 'item', 'categories', 'elementTypes', 'branches', 'models', 'agricultureTypes'],
  components: {
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      subcategories: [],
      groups: [],
      products: [],
      clients: [],
      attachments: [],
      errorValidate: '',
      currentLocale: this.$store.state.currentLocale,
      form: Object.assign({}, this.item),
      languages: vuejs_datepicker_src_locale__WEBPACK_IMPORTED_MODULE_2__,
      isReset: false,
      disabledDates: {// to: new Date(new Date().getFullYear(), new Date().getMonth()-2, 5), // Disable all dates up to specific date
        // from: new Date(new Date().getFullYear(), new Date().getMonth()+2, 26), // Disable all dates after specific date
        // daysOfMonth: [8, 11, 17], // Disable Specific days
      },
      highlightedFn: {
        customPredictor: function customPredictor(date) {
          if (date.getDate() == moment().format('D')) {
            return true;
          }
        }
      },
      position: {
        lat: Number(this.item.latitude),
        lng: Number(this.item.longitude)
      } // markers: [
      //     { position: { lat: Number(this.item.latitude), lng: Number(this.item.longitude) } },
      // ]

    };
  },
  created: function created() {
    console.log('exp form >>>', this.form);
  },
  mounted: function mounted() {
    this.loadAllData(); //   this.form = Object.assign({}, this.item);

    console.log('form>>>', this.form);
  },
  computed: {
    errors: {
      get: function get() {
        return this.errors;
      },
      set: function set(error) {
        return error;
      }
    }
  },
  methods: {
    submitForm: function submitForm() {
      var _this = this;

      this.$validator.validateAll().then(function (result) {
        if (result) {
          if (_this.editMode) _this.updateItem();else _this.createItem();
        } else {// form have errors
        }
      });
    },
    addExperimentCategory: function addExperimentCategory() {
      this.form.categories.experiments.push(this.experimentObj());
    },
    addComparisonCategory: function addComparisonCategory() {
      this.form.categories.comparisons.push(this.comparisonObj());
    },
    removeExperimentCategory: function removeExperimentCategory(key) {
      this.form.categories.experiments.splice(key, 1);
    },
    removeComparisonCategory: function removeComparisonCategory(key) {
      this.form.categories.comparisons.splice(key, 1);
    },
    currentCategory: function currentCategory(value, key, type) {
      var obj = type == 'experiment' ? this.form.categories.experiments[key] : this.form.categories.comparisons[key]; //** reset old value of this block */

      obj.subcategory_id = '';
      obj.group_id = '';
      obj.product_id = '';
      obj.subcategories = [];
      obj.groups = [];
      obj.products = [];
      if (value != '') Promise.all([this.getSubCategories(value)]).then(function (res) {
        obj.subcategories = res[0].response;
      });
    },
    currentSubcategory: function currentSubcategory(value, key, type) {
      var obj = type == 'experiment' ? this.form.categories.experiments[key] : this.form.categories.comparisons[key]; //** reset old value of this block */

      obj.group_id = '';
      obj.product_id = '';
      obj.groups = [];
      obj.products = [];
      if (value != '') Promise.all([this.getGroups(value)]).then(function (res) {
        obj.groups = res[0].response;
      });
    },
    currentGroup: function currentGroup(value, key, type) {
      var obj = type == 'experiment' ? this.form.categories.experiments[key] : this.form.categories.comparisons[key]; //** reset old value of this block */

      obj.product_id = '';
      obj.products = [];
      if (value != '') Promise.all([this.getProducts(value)]).then(function (res) {
        obj.products = res[0].response;
      });
    },
    checkSelectType: function checkSelectType(value, key) {
      console.log(value, key);
      if (value == 5) // select
        this.addElementOption(key);else // reset options
        this.form.elements[key].options = [];
    },
    addElement: function addElement() {
      this.form.elements.push({
        ar: {
          name: ""
        },
        en: {
          name: ""
        },
        element_type_id: '',
        options: [],
        status: true
      });
    },
    removeElement: function removeElement(key) {
      this.form.elements.splice(key, 1);
    },
    addElementOption: function addElementOption(key) {
      this.form.elements[key].options.push({
        name_en: "",
        name_ar: ""
      });
    },
    removeElementOption: function removeElementOption(key, index) {
      this.form.elements[key].options.splice(index, 1);
    },
    navigateRoute: function navigateRoute() {
      this.$router.push({
        name: 'experiments'
      });
    },
    mappingObj: function mappingObj() {
      var obj = this.cloneItem(this.form);
      obj.categories.experiments.forEach(function (category) {
        delete category['subcategories'];
        delete category['groups'];
        delete category['products'];
      });
      obj.categories.comparisons.forEach(function (category) {
        delete category['subcategories'];
        delete category['groups'];
        delete category['products'];
      });
      if (this.isReset) obj.elements.forEach(function (element) {
        delete element['id'];
      });
      return obj;
    },
    createItem: function createItem() {
      var _this2 = this;

      var obj = this.mappingObj();
      axios.post('api/iPanel/experiments', obj).then(function (data) {
        _this2.$vs.notify({
          color: 'success',
          title: 'success',
          text: _this2.$t('success.itemCreated'),
          position: 'top-right'
        });

        _this2.navigateRoute();
      }).catch(function (error) {
        //** A simple way to handle Laravel back-end validation in Vue  */
        if (error.errors) _this2.form['errors']['errors'] = error.errors;
        if (error.error && error.error.message) _this2.errorValidate = error.error.message;
      });
    },
    updateItem: function updateItem() {
      var _this3 = this;

      var obj = this.mappingObj();
      axios.put("api/iPanel/experiments/".concat(this.form.id), obj).then(function (data) {
        _this3.$vs.notify({
          color: 'success',
          title: 'success',
          text: _this3.$t('success.itemUpdated'),
          position: 'top-right'
        }); // this.navigateRoute();

      }).catch(function (error) {
        //** A simple way to handle Laravel back-end validation in Vue  */
        if (error.errors) _this3.form['errors']['errors'] = error.errors;
        if (error.error && error.error.message) _this3.errorValidate = error.error.message;
      });
    },
    // initialData() {
    //     // this.resetForm();
    //     if(this.form && this.form.id) this.form.fill(this.form);
    // },
    resetForm: function resetForm() {
      this.form.clear(); //** Clear the form errors. */

      this.form.reset(); //** Reset the form fields. */
    },
    getCategories: function () {
      var _getCategories = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return axios.get('api/iPanel/categories');

              case 2:
                response = _context.sent;
                return _context.abrupt("return", response);

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function getCategories() {
        return _getCategories.apply(this, arguments);
      }

      return getCategories;
    }(),
    loadAllData: function loadAllData() {
      if (this.editMode) this.loadSelectData();
    },
    loadSelectData: function loadSelectData() {
      var _this4 = this;

      Promise.all([this.getClients()]).then(function (res) {
        _this4.clients = res[0].response;
      });
    },
    getSubCategories: function () {
      var _getSubCategories = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(category_id) {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return axios.get("api/iPanel/subcategories/".concat(category_id));

              case 2:
                response = _context2.sent;
                return _context2.abrupt("return", response);

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function getSubCategories(_x) {
        return _getSubCategories.apply(this, arguments);
      }

      return getSubCategories;
    }(),
    getGroups: function () {
      var _getGroups = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(subcategory_id) {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return axios.get("api/iPanel/groups/".concat(subcategory_id));

              case 2:
                response = _context3.sent;
                return _context3.abrupt("return", response);

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function getGroups(_x2) {
        return _getGroups.apply(this, arguments);
      }

      return getGroups;
    }(),
    getProducts: function () {
      var _getProducts = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4(group_id) {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return axios.get("api/iPanel/get-products/".concat(group_id));

              case 2:
                response = _context4.sent;
                return _context4.abrupt("return", response);

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function getProducts(_x3) {
        return _getProducts.apply(this, arguments);
      }

      return getProducts;
    }(),
    getClients: function () {
      var _getClients = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return axios.get("api/iPanel/get-clients/".concat(this.form.branch_id));

              case 2:
                response = _context5.sent;
                return _context5.abrupt("return", response);

              case 4:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function getClients() {
        return _getClients.apply(this, arguments);
      }

      return getClients;
    }(),
    experimentObj: function experimentObj() {
      var obj = {
        category_id: '',
        subcategory_id: '',
        group_id: '',
        product_id: '',
        quantity: '',
        area: '',
        type: 'experiment',
        subcategories: [],
        groups: [],
        products: []
      };
      return obj;
    },
    comparisonObj: function comparisonObj() {
      var obj = {
        category_id: '',
        subcategory_id: '',
        group_id: '',
        product_id: '',
        type: 'comparison',
        subcategories: [],
        groups: [],
        products: []
      };
      return obj;
    },
    witnessObj: function witnessObj() {
      var obj = {
        witness: '',
        type: 'witness'
      };
      return obj;
    },
    handleMap: function handleMap(event) {
      console.log('center', event.latLng.lat(), event.latLng.lng());
      var lng = Number(event.latLng.lat().toFixed(5));
      var lat = Number(event.latLng.lng().toFixed(5));
      this.position = {
        lat: lat,
        lng: lng
      };
      this.form.latitude = lat;
      this.form.longitude = lng;
    }
  },
  watch: {
    'form.categoryType': function formCategoryType(val, oldVal) {
      if (val == 'witness') {
        this.form.categories.experiments = [];
        this.form.categories.comparisons = [];
        this.form.categories.witness = [this.witnessObj()];
      } else {
        this.form.categories.witness = [];
        this.form.categories.experiments = [this.experimentObj()];
        this.form.categories.comparisons = [this.comparisonObj()];
      }
    },
    'form.branch_id': function formBranch_id(val, oldVal) {
      var _this5 = this;

      //** reset old value */
      this.form.client_id = '';
      if (this.form.branch_id != '') Promise.all([this.getClients()]).then(function (res) {
        _this5.clients = res[0].response;
      });else this.clients = [];
    },
    'form.model_id': function formModel_id(val, oldVal) {
      //** reset old value */
      this.form.elements = [];

      if (this.form.model_id != '') {
        var model = this.models.find(function (model) {
          return model.id == this.form.model_id;
        }, this);
        if (model) this.form.elements = model.elements;
        this.isReset = true;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/single.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/experiments/single.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".vs-switch{\n  width: 80px !important;\n}\n.vs-switch--text {\n  font-size: 1rem !important;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/single.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/experiments/single.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/single.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/single.vue?vue&type=template&id=134db4d4&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/experiments/single.vue?vue&type=template&id=134db4d4& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "page-user-edit" } },
    [
      _c(
        "vx-card",
        {
          attrs: {
            title: !_vm.editMode
              ? _vm.$t("common.create")
              : _vm.$t("common.edit")
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "tabs-container px-6 pt-6",
              attrs: { slot: "no-body" },
              slot: "no-body"
            },
            [
              _c(
                "form",
                [
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required",
                              expression: "'required'"
                            }
                          ],
                          staticClass: "w-full",
                          attrs: {
                            "label-placeholder": _vm.$t("common['en.name']"),
                            name: "en.name"
                          },
                          model: {
                            value: _vm.form.en.name,
                            callback: function($$v) {
                              _vm.$set(_vm.form.en, "name", $$v)
                            },
                            expression: "form.en.name"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("en.name"),
                                expression: "errors.has('en.name')"
                              }
                            ],
                            staticClass: "text-danger text-sm",
                            attrs: { "data-vv-as": "en.name" }
                          },
                          [_vm._v(_vm._s(_vm.errors.first("en.name")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "en.name" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required",
                              expression: "'required'"
                            }
                          ],
                          staticClass: "w-full",
                          attrs: {
                            "label-placeholder": _vm.$t("common['ar.name']"),
                            name: "ar.name"
                          },
                          model: {
                            value: _vm.form.ar.name,
                            callback: function($$v) {
                              _vm.$set(_vm.form.ar, "name", $$v)
                            },
                            expression: "form.ar.name"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("ar.name"),
                                expression: "errors.has('ar.name')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("ar.name")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "ar.name" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required",
                              expression: "'required'"
                            }
                          ],
                          staticClass: "mt-5 w-full",
                          attrs: {
                            "label-placeholder": _vm.$t(
                              "common.codeExperiment"
                            ),
                            name: "codeExperiment"
                          },
                          model: {
                            value: _vm.form.code,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "code", $$v)
                            },
                            expression: "form.code"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("codeExperiment"),
                                expression: "errors.has('codeExperiment')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("codeExperiment")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "code" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required",
                              expression: "'required'"
                            }
                          ],
                          staticClass: "mt-5 w-full",
                          attrs: {
                            "label-placeholder": _vm.$t(
                              "common.responsible_person"
                            ),
                            name: "responsible_person"
                          },
                          model: {
                            value: _vm.form.responsible_person,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "responsible_person", $$v)
                            },
                            expression: "form.responsible_person"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("responsible_person"),
                                expression: "errors.has('responsible_person')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [
                            _vm._v(
                              _vm._s(_vm.errors.first("responsible_person"))
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "responsible_person" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("datepicker", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required",
                              expression: "'required'"
                            }
                          ],
                          staticClass: "mt-5 w-full",
                          attrs: {
                            placeholder: _vm.$t("common.start_date"),
                            highlighted: _vm.highlightedFn,
                            name: "start_date",
                            language: _vm.languages[_vm.currentLocale],
                            format: "d MMMM yyyy"
                          },
                          model: {
                            value: _vm.form.start_date,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "start_date", $$v)
                            },
                            expression: "form.start_date"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("start_date"),
                                expression: "errors.has('start_date')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("start_date")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "start_date" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("datepicker", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required",
                              expression: "'required'"
                            }
                          ],
                          staticClass: "mt-5 w-full",
                          attrs: {
                            placeholder: _vm.$t("common.end_date"),
                            highlighted: _vm.highlightedFn,
                            name: "end_date",
                            disabledDates: _vm.disabledDates,
                            language: _vm.languages[_vm.currentLocale],
                            format: "d MMMM yyyy"
                          },
                          model: {
                            value: _vm.form.end_date,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "end_date", $$v)
                            },
                            expression: "form.end_date"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("end_date"),
                                expression: "errors.has('end_date')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("end_date")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "end_date" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required",
                              expression: "'required'"
                            }
                          ],
                          staticClass: "mt-5 w-full",
                          attrs: {
                            "label-placeholder": _vm.$t("common.purpose"),
                            name: "purpose"
                          },
                          model: {
                            value: _vm.form.purpose,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "purpose", $$v)
                            },
                            expression: "form.purpose"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("purpose"),
                                expression: "errors.has('purpose')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("purpose")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "purpose" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required|numeric",
                              expression: "'required|numeric'"
                            }
                          ],
                          staticClass: "mt-5 w-full",
                          attrs: {
                            "label-placeholder":
                              _vm.$t("common.experimentArea") +
                              " " +
                              _vm.$t("common.areaUnit"),
                            name: "experimentArea"
                          },
                          model: {
                            value: _vm.form.area,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "area", $$v)
                            },
                            expression: "form.area"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("experimentArea"),
                                expression: "errors.has('experimentArea')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("experimentArea")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "area" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required|numeric",
                              expression: "'required|numeric'"
                            }
                          ],
                          staticClass: "mt-5 w-full",
                          attrs: {
                            "label-placeholder": _vm.$t("common.weight"),
                            name: "weight"
                          },
                          model: {
                            value: _vm.form.weight,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "weight", $$v)
                            },
                            expression: "form.weight"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("weight"),
                                expression: "errors.has('weight')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("weight")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "weight" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required",
                              expression: "'required'"
                            }
                          ],
                          staticClass: "mt-5 w-full",
                          attrs: {
                            "label-placeholder": _vm.$t("common.weight_unit"),
                            name: "weight_unit"
                          },
                          model: {
                            value: _vm.form.weight_unit,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "weight_unit", $$v)
                            },
                            expression: "form.weight_unit"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("weight_unit"),
                                expression: "errors.has('weight_unit')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("weight_unit")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "weight_unit" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c(
                          "vs-select",
                          {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "selectExample mt-5 w-full",
                            attrs: { autocomplete: "", name: "branch_id" },
                            model: {
                              value: _vm.form.branch_id,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "branch_id", $$v)
                              },
                              expression: "form.branch_id"
                            }
                          },
                          [
                            _c("vs-select-item", {
                              attrs: {
                                value: "",
                                text: _vm.$t("common.branch_id")
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.branches, function(item, index) {
                              return _c("vs-select-item", {
                                key: index,
                                attrs: {
                                  value: item.id,
                                  text: item[_vm.currentLocale].name
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("branch_id"),
                                expression: "errors.has('branch_id')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("branch_id")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "branch_id" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c(
                          "vs-select",
                          {
                            staticClass: "selectExample mt-5 w-full",
                            attrs: { autocomplete: "", name: "client_id" },
                            model: {
                              value: _vm.form.client_id,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "client_id", $$v)
                              },
                              expression: "form.client_id"
                            }
                          },
                          [
                            _c("vs-select-item", {
                              attrs: {
                                value: "",
                                text: _vm.$t("common.client_id")
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.clients, function(item, index) {
                              return _c("vs-select-item", {
                                key: index,
                                attrs: {
                                  value: item.id,
                                  text: item[_vm.currentLocale].name
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("client_id"),
                                expression: "errors.has('client_id')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("client_id")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "client_id" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c("div", { staticClass: "vx-col w-full mb-2" }, [
                      _c("ul", { staticClass: "demo-alignment" }, [
                        _c(
                          "li",
                          [
                            _c(
                              "vs-radio",
                              {
                                attrs: {
                                  color: "primary",
                                  "vs-value": "categories"
                                },
                                model: {
                                  value: _vm.form.categoryType,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "categoryType", $$v)
                                  },
                                  expression: "form.categoryType"
                                }
                              },
                              [
                                _vm._v(
                                  "\n                                  " +
                                    _vm._s(_vm.$t("common.categories")) +
                                    "\n                              "
                                )
                              ]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "li",
                          [
                            _c(
                              "vs-radio",
                              {
                                attrs: {
                                  color: "success",
                                  "vs-value": "witness"
                                },
                                model: {
                                  value: _vm.form.categoryType,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "categoryType", $$v)
                                  },
                                  expression: "form.categoryType"
                                }
                              },
                              [
                                _vm._v(
                                  "\n                                  " +
                                    _vm._s(_vm.$t("common.witness")) +
                                    "\n                              "
                                )
                              ]
                            )
                          ],
                          1
                        )
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _vm.errorValidate
                    ? _c(
                        "vs-alert",
                        {
                          attrs: {
                            color: "danger",
                            title: _vm.$t("errors.failed")
                          }
                        },
                        [_c("span", [_vm._v(_vm._s(_vm.errorValidate))])]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _c(
                    "vx-card",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.form.categories.experiments.length,
                          expression: "form.categories.experiments.length"
                        }
                      ],
                      attrs: { title: _vm.$t("common.experimentCategories") }
                    },
                    [
                      _vm._l(_vm.form.categories.experiments, function(
                        experimentCategory,
                        key
                      ) {
                        return _c(
                          "div",
                          { key: "experiment" + key, staticClass: "px-3" },
                          [
                            _c("div", { staticClass: "vx-row" }, [
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c(
                                    "vs-select",
                                    {
                                      directives: [
                                        {
                                          name: "validate",
                                          rawName: "v-validate",
                                          value: "required",
                                          expression: "'required'"
                                        }
                                      ],
                                      staticClass: "selectExample mt-5 w-full",
                                      attrs: {
                                        autocomplete: "",
                                        name: "category_id"
                                      },
                                      on: {
                                        change: function($event) {
                                          return _vm.currentCategory(
                                            $event,
                                            key,
                                            "experiment"
                                          )
                                        }
                                      },
                                      model: {
                                        value: experimentCategory.category_id,
                                        callback: function($$v) {
                                          _vm.$set(
                                            experimentCategory,
                                            "category_id",
                                            $$v
                                          )
                                        },
                                        expression:
                                          "experimentCategory.category_id"
                                      }
                                    },
                                    [
                                      _c("vs-select-item", {
                                        attrs: {
                                          value: "",
                                          text: _vm.$t("category.category_id")
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm._l(_vm.categories, function(
                                        item,
                                        index
                                      ) {
                                        return _c("vs-select-item", {
                                          key: index,
                                          attrs: {
                                            value: item.id,
                                            text: item[_vm.currentLocale].name
                                          }
                                        })
                                      })
                                    ],
                                    2
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has("category_id"),
                                          expression:
                                            "errors.has('category_id')"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm"
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(_vm.errors.first("category_id"))
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: {
                                      form: _vm.form,
                                      field: "category_id"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c(
                                    "vs-select",
                                    {
                                      directives: [
                                        {
                                          name: "validate",
                                          rawName: "v-validate",
                                          value: "required",
                                          expression: "'required'"
                                        }
                                      ],
                                      staticClass: "selectExample mt-5 w-full",
                                      attrs: {
                                        autocomplete: "",
                                        name: "subcategory_id"
                                      },
                                      on: {
                                        change: function($event) {
                                          return _vm.currentSubcategory(
                                            $event,
                                            key,
                                            "experiment"
                                          )
                                        }
                                      },
                                      model: {
                                        value:
                                          experimentCategory.subcategory_id,
                                        callback: function($$v) {
                                          _vm.$set(
                                            experimentCategory,
                                            "subcategory_id",
                                            $$v
                                          )
                                        },
                                        expression:
                                          "experimentCategory.subcategory_id"
                                      }
                                    },
                                    [
                                      _c("vs-select-item", {
                                        attrs: {
                                          value: "",
                                          text: _vm.$t(
                                            "category.subcategory_id"
                                          )
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm._l(
                                        experimentCategory.subcategories,
                                        function(item, index) {
                                          return _c("vs-select-item", {
                                            key: index,
                                            attrs: {
                                              value: item.id,
                                              text: item[_vm.currentLocale].name
                                            }
                                          })
                                        }
                                      )
                                    ],
                                    2
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has(
                                            "subcategory_id"
                                          ),
                                          expression:
                                            "errors.has('subcategory_id')"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm"
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.errors.first("subcategory_id")
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: {
                                      form: _vm.form,
                                      field: "subcategory_id"
                                    }
                                  })
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "vx-row" }, [
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c(
                                    "vs-select",
                                    {
                                      directives: [
                                        {
                                          name: "validate",
                                          rawName: "v-validate",
                                          value: "required",
                                          expression: "'required'"
                                        }
                                      ],
                                      staticClass: "selectExample mt-5 w-full",
                                      attrs: {
                                        autocomplete: "",
                                        name: "group_id"
                                      },
                                      on: {
                                        change: function($event) {
                                          return _vm.currentGroup(
                                            $event,
                                            key,
                                            "experiment"
                                          )
                                        }
                                      },
                                      model: {
                                        value: experimentCategory.group_id,
                                        callback: function($$v) {
                                          _vm.$set(
                                            experimentCategory,
                                            "group_id",
                                            $$v
                                          )
                                        },
                                        expression:
                                          "experimentCategory.group_id"
                                      }
                                    },
                                    [
                                      _c("vs-select-item", {
                                        attrs: {
                                          value: "",
                                          text: _vm.$t("category.group_id")
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm._l(
                                        experimentCategory.groups,
                                        function(item, index) {
                                          return _c("vs-select-item", {
                                            key: index,
                                            attrs: {
                                              value: item.id,
                                              text: item[_vm.currentLocale].name
                                            }
                                          })
                                        }
                                      )
                                    ],
                                    2
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has("group_id"),
                                          expression: "errors.has('group_id')"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm"
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(_vm.errors.first("group_id"))
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: { form: _vm.form, field: "group_id" }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c(
                                    "vs-select",
                                    {
                                      directives: [
                                        {
                                          name: "validate",
                                          rawName: "v-validate",
                                          value: "required",
                                          expression: "'required'"
                                        }
                                      ],
                                      staticClass: "selectExample mt-5 w-full",
                                      attrs: {
                                        autocomplete: "",
                                        name: "product_id"
                                      },
                                      model: {
                                        value: experimentCategory.product_id,
                                        callback: function($$v) {
                                          _vm.$set(
                                            experimentCategory,
                                            "product_id",
                                            $$v
                                          )
                                        },
                                        expression:
                                          "experimentCategory.product_id"
                                      }
                                    },
                                    [
                                      _c("vs-select-item", {
                                        attrs: {
                                          value: "",
                                          text: _vm.$t("category.product_id")
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm._l(
                                        experimentCategory.products,
                                        function(item, index) {
                                          return _c("vs-select-item", {
                                            key: index,
                                            attrs: {
                                              value: item.id,
                                              text: item[_vm.currentLocale].name
                                            }
                                          })
                                        }
                                      )
                                    ],
                                    2
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has("product_id"),
                                          expression: "errors.has('product_id')"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm"
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(_vm.errors.first("product_id"))
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: {
                                      form: _vm.form,
                                      field: "product_id"
                                    }
                                  })
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "vx-row" }, [
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c("vs-input", {
                                    directives: [
                                      {
                                        name: "validate",
                                        rawName: "v-validate",
                                        value: "required",
                                        expression: "'required'"
                                      }
                                    ],
                                    staticClass: "mt-5 w-full",
                                    attrs: {
                                      "label-placeholder": _vm.$t(
                                        "common.quantity"
                                      ),
                                      name: "quantity"
                                    },
                                    model: {
                                      value: experimentCategory.quantity,
                                      callback: function($$v) {
                                        _vm.$set(
                                          experimentCategory,
                                          "quantity",
                                          $$v
                                        )
                                      },
                                      expression: "experimentCategory.quantity"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has("quantity"),
                                          expression: "errors.has('quantity')"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm"
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(_vm.errors.first("quantity"))
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: { form: _vm.form, field: "quantity" }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c("vs-input", {
                                    directives: [
                                      {
                                        name: "validate",
                                        rawName: "v-validate",
                                        value: "required|numeric",
                                        expression: "'required|numeric'"
                                      }
                                    ],
                                    staticClass: "mt-5 w-full",
                                    attrs: {
                                      "label-placeholder": _vm.$t(
                                        "common.area"
                                      ),
                                      name: "area"
                                    },
                                    model: {
                                      value: experimentCategory.area,
                                      callback: function($$v) {
                                        _vm.$set(
                                          experimentCategory,
                                          "area",
                                          $$v
                                        )
                                      },
                                      expression: "experimentCategory.area"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has("area"),
                                          expression: "errors.has('area')"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm"
                                    },
                                    [_vm._v(_vm._s(_vm.errors.first("area")))]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: { form: _vm.form, field: "area" }
                                  })
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("vs-button", {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: key,
                                  expression: "key"
                                }
                              ],
                              staticClass: "mb-4 md:mb-0",
                              staticStyle: { margin: "auto" },
                              attrs: {
                                "icon-pack": "feather",
                                type: "border",
                                color: "danger",
                                icon: "icon-trash"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.removeExperimentCategory(key)
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("vs-divider")
                          ],
                          1
                        )
                      }),
                      _vm._v(" "),
                      _c("vs-button", {
                        staticClass: "mb-4 md:mb-0",
                        staticStyle: { margin: "auto" },
                        attrs: {
                          color: "primary",
                          type: "filled",
                          "icon-pack": "feather",
                          icon: "icon-plus"
                        },
                        on: { click: _vm.addExperimentCategory }
                      })
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c(
                    "vx-card",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.form.categories.comparisons.length,
                          expression: "form.categories.comparisons.length"
                        }
                      ],
                      attrs: { title: _vm.$t("common.comparisonCategories") }
                    },
                    [
                      _vm._l(_vm.form.categories.comparisons, function(
                        comparisonCategory,
                        key
                      ) {
                        return _c(
                          "div",
                          { key: "comparison" + key, staticClass: "px-3" },
                          [
                            _c("div", { staticClass: "vx-row" }, [
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c(
                                    "vs-select",
                                    {
                                      directives: [
                                        {
                                          name: "validate",
                                          rawName: "v-validate",
                                          value: "required",
                                          expression: "'required'"
                                        }
                                      ],
                                      staticClass: "selectExample mt-5 w-full",
                                      attrs: {
                                        autocomplete: "",
                                        name: "category_id"
                                      },
                                      on: {
                                        change: function($event) {
                                          return _vm.currentCategory(
                                            $event,
                                            key,
                                            "comparison"
                                          )
                                        }
                                      },
                                      model: {
                                        value: comparisonCategory.category_id,
                                        callback: function($$v) {
                                          _vm.$set(
                                            comparisonCategory,
                                            "category_id",
                                            $$v
                                          )
                                        },
                                        expression:
                                          "comparisonCategory.category_id"
                                      }
                                    },
                                    [
                                      _c("vs-select-item", {
                                        attrs: {
                                          value: "",
                                          text: _vm.$t("category.category_id")
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm._l(_vm.categories, function(
                                        item,
                                        index
                                      ) {
                                        return _c("vs-select-item", {
                                          key: index,
                                          attrs: {
                                            value: item.id,
                                            text: item[_vm.currentLocale].name
                                          }
                                        })
                                      })
                                    ],
                                    2
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has("category_id"),
                                          expression:
                                            "errors.has('category_id')"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm"
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(_vm.errors.first("category_id"))
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: {
                                      form: _vm.form,
                                      field: "category_id"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c(
                                    "vs-select",
                                    {
                                      directives: [
                                        {
                                          name: "validate",
                                          rawName: "v-validate",
                                          value: "required",
                                          expression: "'required'"
                                        }
                                      ],
                                      staticClass: "selectExample mt-5 w-full",
                                      attrs: {
                                        autocomplete: "",
                                        name: "subcategory_id"
                                      },
                                      on: {
                                        change: function($event) {
                                          return _vm.currentSubcategory(
                                            $event,
                                            key,
                                            "comparison"
                                          )
                                        }
                                      },
                                      model: {
                                        value:
                                          comparisonCategory.subcategory_id,
                                        callback: function($$v) {
                                          _vm.$set(
                                            comparisonCategory,
                                            "subcategory_id",
                                            $$v
                                          )
                                        },
                                        expression:
                                          "comparisonCategory.subcategory_id"
                                      }
                                    },
                                    [
                                      _c("vs-select-item", {
                                        attrs: {
                                          value: "",
                                          text: _vm.$t(
                                            "category.subcategory_id"
                                          )
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm._l(
                                        comparisonCategory.subcategories,
                                        function(item, index) {
                                          return _c("vs-select-item", {
                                            key: index,
                                            attrs: {
                                              value: item.id,
                                              text: item[_vm.currentLocale].name
                                            }
                                          })
                                        }
                                      )
                                    ],
                                    2
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has(
                                            "subcategory_id"
                                          ),
                                          expression:
                                            "errors.has('subcategory_id')"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm"
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.errors.first("subcategory_id")
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: {
                                      form: _vm.form,
                                      field: "subcategory_id"
                                    }
                                  })
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "vx-row" }, [
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c(
                                    "vs-select",
                                    {
                                      directives: [
                                        {
                                          name: "validate",
                                          rawName: "v-validate",
                                          value: "required",
                                          expression: "'required'"
                                        }
                                      ],
                                      staticClass: "selectExample mt-5 w-full",
                                      attrs: {
                                        autocomplete: "",
                                        name: "group_id"
                                      },
                                      on: {
                                        change: function($event) {
                                          return _vm.currentGroup(
                                            $event,
                                            key,
                                            "comparison"
                                          )
                                        }
                                      },
                                      model: {
                                        value: comparisonCategory.group_id,
                                        callback: function($$v) {
                                          _vm.$set(
                                            comparisonCategory,
                                            "group_id",
                                            $$v
                                          )
                                        },
                                        expression:
                                          "comparisonCategory.group_id"
                                      }
                                    },
                                    [
                                      _c("vs-select-item", {
                                        attrs: {
                                          value: "",
                                          text: _vm.$t("category.group_id")
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm._l(
                                        comparisonCategory.groups,
                                        function(item, index) {
                                          return _c("vs-select-item", {
                                            key: index,
                                            attrs: {
                                              value: item.id,
                                              text: item[_vm.currentLocale].name
                                            }
                                          })
                                        }
                                      )
                                    ],
                                    2
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has("group_id"),
                                          expression: "errors.has('group_id')"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm"
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(_vm.errors.first("group_id"))
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: { form: _vm.form, field: "group_id" }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c(
                                    "vs-select",
                                    {
                                      directives: [
                                        {
                                          name: "validate",
                                          rawName: "v-validate",
                                          value: "required",
                                          expression: "'required'"
                                        }
                                      ],
                                      staticClass: "selectExample mt-5 w-full",
                                      attrs: {
                                        autocomplete: "",
                                        name: "product_id"
                                      },
                                      model: {
                                        value: comparisonCategory.product_id,
                                        callback: function($$v) {
                                          _vm.$set(
                                            comparisonCategory,
                                            "product_id",
                                            $$v
                                          )
                                        },
                                        expression:
                                          "comparisonCategory.product_id"
                                      }
                                    },
                                    [
                                      _c("vs-select-item", {
                                        attrs: {
                                          value: "",
                                          text: _vm.$t("category.product_id")
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm._l(
                                        comparisonCategory.products,
                                        function(item, index) {
                                          return _c("vs-select-item", {
                                            key: index,
                                            attrs: {
                                              value: item.id,
                                              text: item[_vm.currentLocale].name
                                            }
                                          })
                                        }
                                      )
                                    ],
                                    2
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has("product_id"),
                                          expression: "errors.has('product_id')"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm"
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(_vm.errors.first("product_id"))
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: {
                                      form: _vm.form,
                                      field: "product_id"
                                    }
                                  })
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("vs-button", {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: key,
                                  expression: "key"
                                }
                              ],
                              staticClass: "mb-4 md:mb-0",
                              staticStyle: { margin: "auto" },
                              attrs: {
                                "icon-pack": "feather",
                                type: "border",
                                color: "danger",
                                icon: "icon-trash"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.removeComparisonCategory(key)
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("vs-divider")
                          ],
                          1
                        )
                      }),
                      _vm._v(" "),
                      _c("vs-button", {
                        staticClass: "mb-4 md:mb-0",
                        staticStyle: { margin: "auto" },
                        attrs: {
                          color: "primary",
                          type: "filled",
                          "icon-pack": "feather",
                          icon: "icon-plus"
                        },
                        on: { click: _vm.addComparisonCategory }
                      })
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c(
                    "vx-card",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.form.categories.witness.length,
                          expression: "form.categories.witness.length"
                        }
                      ],
                      attrs: { title: _vm.$t("common.witnessCategories") }
                    },
                    _vm._l(_vm.form.categories.witness, function(
                      witnessCategory,
                      key
                    ) {
                      return _c(
                        "div",
                        { key: "witness" + key, staticClass: "px-3" },
                        [
                          _c("div", { staticClass: "vx-row" }, [
                            _c(
                              "div",
                              { staticClass: "vx-col w-full mb-2" },
                              [
                                _c("vs-input", {
                                  directives: [
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: "required",
                                      expression: "'required'"
                                    }
                                  ],
                                  staticClass: "mt-5 w-full",
                                  attrs: {
                                    "label-placeholder": _vm.$t(
                                      "common.witness"
                                    ),
                                    name: "witness"
                                  },
                                  model: {
                                    value: witnessCategory.witness,
                                    callback: function($$v) {
                                      _vm.$set(witnessCategory, "witness", $$v)
                                    },
                                    expression: "witnessCategory.witness"
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: _vm.errors.has("witness"),
                                        expression: "errors.has('witness')"
                                      }
                                    ],
                                    staticClass: "text-danger text-sm"
                                  },
                                  [_vm._v(_vm._s(_vm.errors.first("witness")))]
                                ),
                                _vm._v(" "),
                                _c("has-error", {
                                  staticClass: "text-danger text-sm",
                                  attrs: { form: _vm.form, field: "witness" }
                                })
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c("vs-divider")
                        ],
                        1
                      )
                    }),
                    0
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c(
                          "vs-select",
                          {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "selectExample mt-5 w-full",
                            attrs: {
                              autocomplete: "",
                              name: "agriculture_type_id"
                            },
                            model: {
                              value: _vm.form.agriculture_type_id,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "agriculture_type_id", $$v)
                              },
                              expression: "form.agriculture_type_id"
                            }
                          },
                          [
                            _c("vs-select-item", {
                              attrs: {
                                value: "",
                                text: _vm.$t("common.agriculture_type_id")
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.agricultureTypes, function(item, index) {
                              return _c("vs-select-item", {
                                key: index,
                                attrs: {
                                  value: item.id,
                                  text: item[_vm.currentLocale].name
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("agriculture_type_id"),
                                expression: "errors.has('agriculture_type_id')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [
                            _vm._v(
                              _vm._s(_vm.errors.first("agriculture_type_id"))
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: {
                            form: _vm.form,
                            field: "agriculture_type_id"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c(
                          "vs-select",
                          {
                            staticClass: "selectExample mt-5 w-full",
                            attrs: { autocomplete: "", name: "model_id" },
                            model: {
                              value: _vm.form.model_id,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "model_id", $$v)
                              },
                              expression: "form.model_id"
                            }
                          },
                          [
                            _c("vs-select-item", {
                              attrs: {
                                value: "",
                                text: _vm.$t("common.model_id")
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.models, function(item, index) {
                              return _c("vs-select-item", {
                                key: index,
                                attrs: {
                                  value: item.id,
                                  text: item[_vm.currentLocale].name
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("model_id"),
                                expression: "errors.has('model_id')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("model_id")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "model_id" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "vx-card",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.form.elements.length,
                          expression: "form.elements.length"
                        }
                      ],
                      attrs: { title: _vm.$t("common.modelElements") }
                    },
                    [
                      _vm._l(_vm.form.elements, function(element, key) {
                        return _c(
                          "div",
                          { key: "element" + key, staticClass: "px-3" },
                          [
                            _c("div", { staticClass: "vx-row" }, [
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c("vs-input", {
                                    directives: [
                                      {
                                        name: "validate",
                                        rawName: "v-validate",
                                        value: "required",
                                        expression: "'required'"
                                      }
                                    ],
                                    staticClass: "w-full",
                                    attrs: {
                                      "label-placeholder": _vm.$t(
                                        "common['elements.en.name']"
                                      ),
                                      name: "element_" + key + "_en_name"
                                    },
                                    model: {
                                      value: element.en.name,
                                      callback: function($$v) {
                                        _vm.$set(element.en, "name", $$v)
                                      },
                                      expression: "element.en.name"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has(
                                            "element_" + key + "_en_name"
                                          ),
                                          expression:
                                            "errors.has(`element_${key}_en_name`)"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm",
                                      attrs: { "data-vv-as": element.en.name }
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.errors.first(
                                            "element_" + key + "_en_name"
                                          )
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: {
                                      form: _vm.form,
                                      field: "elements." + key + ".en.name"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c("vs-input", {
                                    directives: [
                                      {
                                        name: "validate",
                                        rawName: "v-validate",
                                        value: "required",
                                        expression: "'required'"
                                      }
                                    ],
                                    staticClass: "w-full",
                                    attrs: {
                                      "label-placeholder": _vm.$t(
                                        "common['elements.ar.name']"
                                      ),
                                      name: "element_" + key + "_ar_name"
                                    },
                                    model: {
                                      value: element.ar.name,
                                      callback: function($$v) {
                                        _vm.$set(element.ar, "name", $$v)
                                      },
                                      expression: "element.ar.name"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has(
                                            "element_" + key + "_ar_name"
                                          ),
                                          expression:
                                            "errors.has(`element_${key}_ar_name`)"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm"
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.errors.first(
                                            "element_" + key + "_ar_name"
                                          )
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: {
                                      form: _vm.form,
                                      field: "elements." + key + ".ar.name"
                                    }
                                  })
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "vx-row" }, [
                              _c(
                                "div",
                                { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                                [
                                  _c(
                                    "vs-select",
                                    {
                                      directives: [
                                        {
                                          name: "validate",
                                          rawName: "v-validate",
                                          value: "required",
                                          expression: "'required'"
                                        }
                                      ],
                                      staticClass: "selectExample mt-5 w-full",
                                      attrs: {
                                        autocomplete: "",
                                        name: "element_" + key + "_type"
                                      },
                                      on: {
                                        change: function($event) {
                                          return _vm.checkSelectType(
                                            $event,
                                            key
                                          )
                                        }
                                      },
                                      model: {
                                        value: element.element_type_id,
                                        callback: function($$v) {
                                          _vm.$set(
                                            element,
                                            "element_type_id",
                                            $$v
                                          )
                                        },
                                        expression: "element.element_type_id"
                                      }
                                    },
                                    [
                                      _c("vs-select-item", {
                                        attrs: {
                                          value: "",
                                          text: _vm.$t("common.type")
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm._l(_vm.elementTypes, function(
                                        item,
                                        index
                                      ) {
                                        return _c("vs-select-item", {
                                          key: index,
                                          attrs: {
                                            value: item.id,
                                            text: item[_vm.currentLocale].name
                                          }
                                        })
                                      })
                                    ],
                                    2
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.errors.has(
                                            "element_" + key + "_type"
                                          ),
                                          expression:
                                            "errors.has(`element_${key}_type`)"
                                        }
                                      ],
                                      staticClass: "text-danger text-sm"
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.errors.first(
                                            "element_" + key + "_type"
                                          )
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    staticClass: "text-danger text-sm",
                                    attrs: {
                                      form: _vm.form,
                                      field:
                                        "elements." + key + ".element_type_id"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "vx-col sm:w-1/2 w-full mb-2 mt-8"
                                },
                                [
                                  _c(
                                    "vs-switch",
                                    {
                                      attrs: { color: "success" },
                                      model: {
                                        value: element.status,
                                        callback: function($$v) {
                                          _vm.$set(element, "status", $$v)
                                        },
                                        expression: "element.status"
                                      }
                                    },
                                    [
                                      _c(
                                        "span",
                                        { attrs: { slot: "on" }, slot: "on" },
                                        [
                                          _vm._v(
                                            _vm._s(_vm.$t("common.required"))
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        { attrs: { slot: "off" }, slot: "off" },
                                        [
                                          _vm._v(
                                            _vm._s(_vm.$t("common.optional"))
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "vx-card",
                              {
                                directives: [
                                  {
                                    name: "show",
                                    rawName: "v-show",
                                    value: element.options.length,
                                    expression: "element.options.length"
                                  }
                                ],
                                attrs: {
                                  title: _vm.$t("common.modelElementOptions")
                                }
                              },
                              [
                                _vm._l(element.options, function(
                                  elementOption,
                                  index
                                ) {
                                  return _c(
                                    "div",
                                    {
                                      key: "element" + index,
                                      staticClass: "px-3"
                                    },
                                    [
                                      _c("div", { staticClass: "vx-row" }, [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "vx-col sm:w-1/2 w-full mb-2"
                                          },
                                          [
                                            _c("vs-input", {
                                              directives: [
                                                {
                                                  name: "validate",
                                                  rawName: "v-validate",
                                                  value: "required",
                                                  expression: "'required'"
                                                }
                                              ],
                                              staticClass: "w-full",
                                              attrs: {
                                                "label-placeholder": _vm.$t(
                                                  "common['options.en.name']"
                                                ),
                                                name:
                                                  "element_" +
                                                  key +
                                                  "_option_" +
                                                  index +
                                                  "_en_name"
                                              },
                                              model: {
                                                value: elementOption.name_en,
                                                callback: function($$v) {
                                                  _vm.$set(
                                                    elementOption,
                                                    "name_en",
                                                    $$v
                                                  )
                                                },
                                                expression:
                                                  "elementOption.name_en"
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "span",
                                              {
                                                directives: [
                                                  {
                                                    name: "show",
                                                    rawName: "v-show",
                                                    value: _vm.errors.has(
                                                      "element_" +
                                                        key +
                                                        "_option_" +
                                                        index +
                                                        "_en_name"
                                                    ),
                                                    expression:
                                                      "errors.has(`element_${key}_option_${index}_en_name`)"
                                                  }
                                                ],
                                                staticClass:
                                                  "text-danger text-sm"
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    _vm.errors.first(
                                                      "element_" +
                                                        key +
                                                        "_option_" +
                                                        index +
                                                        "_en_name"
                                                    )
                                                  )
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c("has-error", {
                                              staticClass:
                                                "text-danger text-sm",
                                              attrs: {
                                                form: _vm.form,
                                                field:
                                                  "elements." +
                                                  key +
                                                  ".options." +
                                                  index +
                                                  ".name_en"
                                              }
                                            })
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "vx-col sm:w-1/2 w-full mb-2"
                                          },
                                          [
                                            _c("vs-input", {
                                              directives: [
                                                {
                                                  name: "validate",
                                                  rawName: "v-validate",
                                                  value: "required",
                                                  expression: "'required'"
                                                }
                                              ],
                                              staticClass: "w-full",
                                              attrs: {
                                                "label-placeholder": _vm.$t(
                                                  "common['options.ar.name']"
                                                ),
                                                name:
                                                  "element_" +
                                                  key +
                                                  "_option_" +
                                                  index +
                                                  "_ar_name"
                                              },
                                              model: {
                                                value: elementOption.name_ar,
                                                callback: function($$v) {
                                                  _vm.$set(
                                                    elementOption,
                                                    "name_ar",
                                                    $$v
                                                  )
                                                },
                                                expression:
                                                  "elementOption.name_ar"
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "span",
                                              {
                                                directives: [
                                                  {
                                                    name: "show",
                                                    rawName: "v-show",
                                                    value: _vm.errors.has(
                                                      "element_" +
                                                        key +
                                                        "_option_" +
                                                        index +
                                                        "_ar_name"
                                                    ),
                                                    expression:
                                                      "errors.has(`element_${key}_option_${index}_ar_name`)"
                                                  }
                                                ],
                                                staticClass:
                                                  "text-danger text-sm"
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    _vm.errors.first(
                                                      "element_" +
                                                        key +
                                                        "_option_" +
                                                        index +
                                                        "_ar_name"
                                                    )
                                                  )
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c("has-error", {
                                              staticClass:
                                                "text-danger text-sm",
                                              attrs: {
                                                form: _vm.form,
                                                field:
                                                  "elements." +
                                                  key +
                                                  ".options." +
                                                  index +
                                                  ".name_ar"
                                              }
                                            })
                                          ],
                                          1
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("vs-button", {
                                        directives: [
                                          {
                                            name: "show",
                                            rawName: "v-show",
                                            value: index,
                                            expression: "index"
                                          }
                                        ],
                                        staticClass: "mb-4 md:mb-0",
                                        staticStyle: { margin: "auto" },
                                        attrs: {
                                          "icon-pack": "feather",
                                          type: "border",
                                          color: "danger",
                                          icon: "icon-trash"
                                        },
                                        on: {
                                          click: function($event) {
                                            return _vm.removeElementOption(
                                              key,
                                              index
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("vs-divider")
                                    ],
                                    1
                                  )
                                }),
                                _vm._v(" "),
                                _c("vs-button", {
                                  staticClass: "mb-4 md:mb-0",
                                  staticStyle: { margin: "auto" },
                                  attrs: {
                                    color: "primary",
                                    type: "filled",
                                    "icon-pack": "feather",
                                    icon: "icon-plus"
                                  },
                                  on: {
                                    click: function($event) {
                                      return _vm.addElementOption(key)
                                    }
                                  }
                                })
                              ],
                              2
                            ),
                            _vm._v(" "),
                            _c("vs-button", {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: key,
                                  expression: "key"
                                }
                              ],
                              staticClass: "mb-4 md:mb-0",
                              staticStyle: { margin: "auto" },
                              attrs: {
                                "icon-pack": "feather",
                                type: "border",
                                color: "danger",
                                icon: "icon-trash"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.removeElement(key)
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("vs-divider")
                          ],
                          1
                        )
                      }),
                      _vm._v(" "),
                      _c("vs-button", {
                        staticClass: "mb-4 md:mb-0",
                        staticStyle: { margin: "auto" },
                        attrs: {
                          color: "primary",
                          type: "filled",
                          "icon-pack": "feather",
                          icon: "icon-plus"
                        },
                        on: { click: _vm.addElement }
                      })
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required|numeric",
                              expression: "'required|numeric'"
                            }
                          ],
                          staticClass: "mt-5 w-full",
                          attrs: {
                            "label-placeholder": _vm.$t("common.production"),
                            name: "production"
                          },
                          model: {
                            value: _vm.form.production,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "production", $$v)
                            },
                            expression: "form.production"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("production"),
                                expression: "errors.has('production')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("production")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "production" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col w-full mb-2" },
                      [
                        _c("vs-textarea", {
                          staticClass: "mt-5 w-full",
                          attrs: {
                            label: _vm.$t("common['en.expDescription']"),
                            name: "en.expDescription"
                          },
                          model: {
                            value: _vm.form.en.description,
                            callback: function($$v) {
                              _vm.$set(_vm.form.en, "description", $$v)
                            },
                            expression: "form.en.description"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("en.expDescription"),
                                expression: "errors.has('en.expDescription')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [
                            _vm._v(
                              _vm._s(_vm.errors.first("en.expDescription"))
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "en.description" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col w-full mb-2" },
                      [
                        _c("vs-textarea", {
                          staticClass: "mt-5 w-full",
                          attrs: {
                            label: _vm.$t("common['ar.expDescription']"),
                            name: "ar.expDescription"
                          },
                          model: {
                            value: _vm.form.ar.description,
                            callback: function($$v) {
                              _vm.$set(_vm.form.ar, "description", $$v)
                            },
                            expression: "form.ar.description"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("ar.expDescription"),
                                expression: "errors.has('ar.expDescription')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [
                            _vm._v(
                              _vm._s(_vm.errors.first("ar.expDescription"))
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "ar.description" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "mt-5" },
                    [
                      _c(
                        "GmapMap",
                        {
                          staticStyle: { width: "100%", height: "400px" },
                          attrs: {
                            center: _vm.position,
                            zoom: 7,
                            "map-type-id": "terrain"
                          },
                          on: { click: _vm.handleMap }
                        },
                        [
                          _c("GmapMarker", {
                            attrs: {
                              position: _vm.position,
                              clickable: true,
                              draggable: true
                            },
                            on: { click: _vm.position, dragend: _vm.handleMap }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("vs-divider"),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-2/3 w-full ml-auto" },
                      [
                        _c(
                          "vs-button",
                          {
                            staticClass: "mr-3 mb-2",
                            attrs: { type: "filled" },
                            on: {
                              click: function($event) {
                                $event.preventDefault()
                                return _vm.submitForm($event)
                              }
                            }
                          },
                          [_vm._v(_vm._s(_vm.$t("common.submit")))]
                        ),
                        _vm._v(" "),
                        _c(
                          "vs-button",
                          {
                            staticClass: "mb-2",
                            attrs: { color: "warning", type: "border" },
                            on: { click: _vm.navigateRoute }
                          },
                          [_vm._v(_vm._s(_vm.$t("common.cancel")))]
                        )
                      ],
                      1
                    )
                  ])
                ],
                1
              )
            ]
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/pages/experiments/single.vue":
/*!*************************************************************!*\
  !*** ./resources/js/src/views/pages/experiments/single.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _single_vue_vue_type_template_id_134db4d4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./single.vue?vue&type=template&id=134db4d4& */ "./resources/js/src/views/pages/experiments/single.vue?vue&type=template&id=134db4d4&");
/* harmony import */ var _single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./single.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/experiments/single.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./single.vue?vue&type=style&index=0&lang=css& */ "./resources/js/src/views/pages/experiments/single.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _single_vue_vue_type_template_id_134db4d4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _single_vue_vue_type_template_id_134db4d4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/experiments/single.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/experiments/single.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/src/views/pages/experiments/single.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/single.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/experiments/single.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/src/views/pages/experiments/single.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/single.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/pages/experiments/single.vue?vue&type=template&id=134db4d4&":
/*!********************************************************************************************!*\
  !*** ./resources/js/src/views/pages/experiments/single.vue?vue&type=template&id=134db4d4& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_134db4d4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=template&id=134db4d4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/experiments/single.vue?vue&type=template&id=134db4d4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_134db4d4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_134db4d4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);