(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[29],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/products/single.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/products/single.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['editMode', 'item'],
  data: function data() {
    return {
      categories: [],
      subcategories: [],
      groups: [],
      countries: [],
      producers: [],
      distributers: [],
      attachments: [],
      currentLocale: this.$store.state.currentLocale,
      form: Object.assign({}, this.item)
    };
  },
  created: function created() {},
  mounted: function mounted() {
    this.loadAllData(); //   this.form = Object.assign({}, this.item);

    console.log('form>>>', this.form);
  },
  computed: {
    errors: {
      get: function get() {
        return this.errors;
      },
      set: function set(error) {
        return error;
      }
    }
  },
  methods: {
    uploadAttachment: function uploadAttachment(event) {
      console.log('upload', event.target.files);
      this.attachments = event.target.files;
    },
    submitForm: function submitForm() {
      var _this = this;

      this.$validator.validateAll().then(function (result) {
        if (result) {
          if (_this.editMode) _this.updateItem();else _this.createItem();
        } else {// form have errors
        }
      });
    },
    navigateRoute: function navigateRoute() {
      this.$router.push({
        name: 'products'
      });
    },
    createItem: function createItem() {
      var _this2 = this;

      axios.post('api/iPanel/products', this.form).then(function (data) {
        _this2.$vs.notify({
          color: 'success',
          title: 'success',
          text: _this2.$t('success.itemCreated'),
          position: 'top-right'
        });

        _this2.navigateRoute();
      }).catch(function (error) {
        //** A simple way to handle Laravel back-end validation in Vue  */
        _this2.form['errors']['errors'] = error.errors;
      });
    },
    updateItem: function updateItem() {
      var _this3 = this;

      axios.put("api/iPanel/products/".concat(this.form.id), this.form).then(function (data) {
        // data = data.response;
        // Fire.$emit('update-item', data);
        _this3.$vs.notify({
          color: 'success',
          title: 'success',
          text: _this3.$t('success.itemUpdated'),
          position: 'top-right'
        });

        _this3.navigateRoute();
      }).catch(function (error) {
        //** A simple way to handle Laravel back-end validation in Vue  */
        _this3.form['errors']['errors'] = error.errors;
      });
    },
    // initialData() {
    //     // this.resetForm();
    //     if(this.form && this.form.id) this.form.fill(this.form);
    // },
    resetForm: function resetForm() {
      this.form.clear(); //** Clear the form errors. */

      this.form.reset(); //** Reset the form fields. */
    },
    getCountries: function () {
      var _getCountries = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return axios.get('api/iPanel/countries');

              case 2:
                response = _context.sent;
                return _context.abrupt("return", response);

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function getCountries() {
        return _getCountries.apply(this, arguments);
      }

      return getCountries;
    }(),
    getCategories: function () {
      var _getCategories = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return axios.get('api/iPanel/categories');

              case 2:
                response = _context2.sent;
                return _context2.abrupt("return", response);

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function getCategories() {
        return _getCategories.apply(this, arguments);
      }

      return getCategories;
    }(),
    getProducers: function () {
      var _getProducers = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return axios.get('api/iPanel/producer-companies');

              case 2:
                response = _context3.sent;
                return _context3.abrupt("return", response);

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function getProducers() {
        return _getProducers.apply(this, arguments);
      }

      return getProducers;
    }(),
    getDistributers: function () {
      var _getDistributers = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return axios.get('api/iPanel/distributed-companies');

              case 2:
                response = _context4.sent;
                return _context4.abrupt("return", response);

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function getDistributers() {
        return _getDistributers.apply(this, arguments);
      }

      return getDistributers;
    }(),
    loadAllData: function loadAllData() {
      var _this4 = this;

      Promise.all([this.getCategories(), this.getCountries(), this.getProducers(), this.getDistributers()]).then(function (res) {
        _this4.categories = res[0].response;
        _this4.countries = res[1].response;
        _this4.producers = res[2].response;
        _this4.distributers = res[3].response;
        if (_this4.editMode) _this4.loadSelectData();
      });
    },
    loadSelectData: function loadSelectData() {
      var _this5 = this;

      Promise.all([this.getSubCategories(), this.getGroups()]).then(function (res) {
        _this5.subcategories = res[0].response;
        _this5.groups = res[1].response;
      });
    },
    getSubCategories: function () {
      var _getSubCategories = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return axios.get("api/iPanel/subcategories/".concat(this.form.category_id));

              case 2:
                response = _context5.sent;
                return _context5.abrupt("return", response);

              case 4:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function getSubCategories() {
        return _getSubCategories.apply(this, arguments);
      }

      return getSubCategories;
    }(),
    getGroups: function () {
      var _getGroups = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return axios.get("api/iPanel/groups/".concat(this.form.subcategory_id));

              case 2:
                response = _context6.sent;
                return _context6.abrupt("return", response);

              case 4:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function getGroups() {
        return _getGroups.apply(this, arguments);
      }

      return getGroups;
    }()
  },
  watch: {
    'form.category_id': function formCategory_id(val, oldVal) {
      var _this6 = this;

      //** reset old value */
      this.form.subcategory_id = '';
      this.form.group_id = '';
      if (this.form.category_id != '') axios.get("api/iPanel/subcategories/".concat(this.form.category_id)).then(function (data) {
        return _this6.subcategories = data.response;
      });else this.subcategories = [];
    },
    'form.subcategory_id': function formSubcategory_id(val, oldVal) {
      var _this7 = this;

      //** reset old value */
      this.form.group_id = '';
      if (this.form.subcategory_id != '') axios.get("api/iPanel/groups/".concat(this.form.subcategory_id)).then(function (data) {
        return _this7.groups = data.response;
      });else this.groups = [];
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/products/single.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/products/single.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".vs-switch{\n  width: 80px !important;\n}\n.vs-switch--text {\n  font-size: 1rem !important;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/products/single.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/products/single.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/products/single.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/products/single.vue?vue&type=template&id=3032fa6e&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/products/single.vue?vue&type=template&id=3032fa6e& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "page-user-edit" } },
    [
      _c(
        "vx-card",
        {
          attrs: {
            title: !_vm.editMode
              ? _vm.$t("common.create")
              : _vm.$t("common.edit")
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "tabs-container px-6 pt-6",
              attrs: { slot: "no-body" },
              slot: "no-body"
            },
            [
              _c(
                "form",
                [
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required",
                              expression: "'required'"
                            }
                          ],
                          staticClass: "w-full",
                          attrs: {
                            "label-placeholder": _vm.$t(
                              "common['en.productName']"
                            ),
                            name: "en.productName"
                          },
                          model: {
                            value: _vm.form.en.name,
                            callback: function($$v) {
                              _vm.$set(_vm.form.en, "name", $$v)
                            },
                            expression: "form.en.name"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("en.productName"),
                                expression: "errors.has('en.productName')"
                              }
                            ],
                            staticClass: "text-danger text-sm",
                            attrs: { "data-vv-as": "en.name" }
                          },
                          [_vm._v(_vm._s(_vm.errors.first("en.productName")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "en.name" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required",
                              expression: "'required'"
                            }
                          ],
                          staticClass: "w-full",
                          attrs: {
                            "label-placeholder": _vm.$t(
                              "common['ar.productName']"
                            ),
                            name: "ar.productName"
                          },
                          model: {
                            value: _vm.form.ar.name,
                            callback: function($$v) {
                              _vm.$set(_vm.form.ar, "name", $$v)
                            },
                            expression: "form.ar.name"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("ar.productName"),
                                expression: "errors.has('ar.productName')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("ar.productName")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "ar.name" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col w-full mb-2" },
                      [
                        _c(
                          "vs-select",
                          {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "selectExample mt-5 w-full",
                            attrs: { autocomplete: "", name: "category_id" },
                            model: {
                              value: _vm.form.category_id,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "category_id", $$v)
                              },
                              expression: "form.category_id"
                            }
                          },
                          [
                            _c("vs-select-item", {
                              attrs: {
                                value: "",
                                text: _vm.$t("category.category_id")
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.categories, function(item, index) {
                              return _c("vs-select-item", {
                                key: index,
                                attrs: {
                                  value: item.id,
                                  text: item[_vm.currentLocale].name
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("category_id"),
                                expression: "errors.has('category_id')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("category_id")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "category_id" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c(
                          "vs-select",
                          {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "selectExample mt-5 w-full",
                            attrs: { autocomplete: "", name: "subcategory_id" },
                            model: {
                              value: _vm.form.subcategory_id,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "subcategory_id", $$v)
                              },
                              expression: "form.subcategory_id"
                            }
                          },
                          [
                            _c("vs-select-item", {
                              attrs: {
                                value: "",
                                text: _vm.$t("category.subcategory_id")
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.subcategories, function(item, index) {
                              return _c("vs-select-item", {
                                key: index,
                                attrs: {
                                  value: item.id,
                                  text: item[_vm.currentLocale].name
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("subcategory_id"),
                                expression: "errors.has('subcategory_id')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("subcategory_id")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "subcategory_id" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c(
                          "vs-select",
                          {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "selectExample mt-5 w-full",
                            attrs: { autocomplete: "", name: "group_id" },
                            model: {
                              value: _vm.form.group_id,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "group_id", $$v)
                              },
                              expression: "form.group_id"
                            }
                          },
                          [
                            _c("vs-select-item", {
                              attrs: {
                                value: "",
                                text: _vm.$t("category.group_id")
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.groups, function(item, index) {
                              return _c("vs-select-item", {
                                key: index,
                                attrs: {
                                  value: item.id,
                                  text: item[_vm.currentLocale].name
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("group_id"),
                                expression: "errors.has('group_id')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("group_id")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "group_id" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c(
                          "vs-select",
                          {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "selectExample mt-5 w-full",
                            attrs: {
                              autocomplete: "",
                              name: "producer_company_id"
                            },
                            model: {
                              value: _vm.form.producer_company_id,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "producer_company_id", $$v)
                              },
                              expression: "form.producer_company_id"
                            }
                          },
                          [
                            _c("vs-select-item", {
                              attrs: {
                                value: "",
                                text: _vm.$t("category.producer_company_id")
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.producers, function(item, index) {
                              return _c("vs-select-item", {
                                key: index,
                                attrs: {
                                  value: item.id,
                                  text: item[_vm.currentLocale].name
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("producer_company_id"),
                                expression: "errors.has('producer_company_id')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [
                            _vm._v(
                              _vm._s(_vm.errors.first("producer_company_id"))
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: {
                            form: _vm.form,
                            field: "producer_company_id"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c(
                          "vs-select",
                          {
                            staticClass: "selectExample mt-5 w-full",
                            attrs: {
                              autocomplete: "",
                              name: "distributed_company_id"
                            },
                            model: {
                              value: _vm.form.distributed_company_id,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.form,
                                  "distributed_company_id",
                                  $$v
                                )
                              },
                              expression: "form.distributed_company_id"
                            }
                          },
                          [
                            _c("vs-select-item", {
                              attrs: {
                                value: "",
                                text: _vm.$t("category.distributed_company_id"),
                                selected: "selected"
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.distributers, function(item, index) {
                              return _c("vs-select-item", {
                                key: index,
                                attrs: {
                                  value: item.id,
                                  text: item[_vm.currentLocale].name
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("distributed_company_id"),
                                expression:
                                  "errors.has('distributed_company_id')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [
                            _vm._v(
                              _vm._s(_vm.errors.first("distributed_company_id"))
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: {
                            form: _vm.form,
                            field: "distributed_company_id"
                          }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c(
                          "vs-select",
                          {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "selectExample mt-5 w-full",
                            attrs: { autocomplete: "", name: "country_id" },
                            model: {
                              value: _vm.form.country_id,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "country_id", $$v)
                              },
                              expression: "form.country_id"
                            }
                          },
                          [
                            _c("vs-select-item", {
                              attrs: {
                                value: "",
                                text: _vm.$t("company.country_id")
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.countries, function(item, index) {
                              return _c("vs-select-item", {
                                key: index,
                                attrs: {
                                  value: item.id,
                                  text: item[_vm.currentLocale].name
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("country_id"),
                                expression: "errors.has('country_id')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("country_id")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "country_id" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          directives: [
                            {
                              name: "validate",
                              rawName: "v-validate",
                              value: "required",
                              expression: "'required'"
                            }
                          ],
                          staticClass: "mt-5 w-full",
                          attrs: {
                            "label-placeholder": _vm.$t("category.productUnit"),
                            name: "productUnit"
                          },
                          model: {
                            value: _vm.form.unit,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "unit", $$v)
                            },
                            expression: "form.unit"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("productUnit"),
                                expression: "errors.has('productUnit')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("productUnit")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "unit" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          staticClass: "mt-5 w-full",
                          attrs: {
                            "label-placeholder": _vm.$t("category.yasin_code"),
                            name: "yasin_code"
                          },
                          model: {
                            value: _vm.form.yasin_code,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "yasin_code", $$v)
                            },
                            expression: "form.yasin_code"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("yasin_code"),
                                expression: "errors.has('yasin_code')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("yasin_code")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "yasin_code" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                      [
                        _c("vs-input", {
                          staticClass: "mt-5 w-full",
                          attrs: {
                            "label-placeholder": _vm.$t(
                              "category.company_code"
                            ),
                            name: "company_code"
                          },
                          model: {
                            value: _vm.form.company_code,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "company_code", $$v)
                            },
                            expression: "form.company_code"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("company_code"),
                                expression: "errors.has('company_code')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [_vm._v(_vm._s(_vm.errors.first("company_code")))]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "company_code" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col w-full mb-2" },
                      [
                        _c("vs-textarea", {
                          staticClass: "mt-5 w-full",
                          attrs: {
                            label: _vm.$t("common['en.productDescription']"),
                            name: "en.productDescription"
                          },
                          model: {
                            value: _vm.form.en.description,
                            callback: function($$v) {
                              _vm.$set(_vm.form.en, "description", $$v)
                            },
                            expression: "form.en.description"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("en.productDescription"),
                                expression:
                                  "errors.has('en.productDescription')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [
                            _vm._v(
                              _vm._s(_vm.errors.first("en.productDescription"))
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "en.description" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "vx-col w-full mb-2" },
                      [
                        _c("vs-textarea", {
                          staticClass: "mt-5 w-full",
                          attrs: {
                            label: _vm.$t("common['ar.productDescription']"),
                            name: "ar.productDescription"
                          },
                          model: {
                            value: _vm.form.ar.description,
                            callback: function($$v) {
                              _vm.$set(_vm.form.ar, "description", $$v)
                            },
                            expression: "form.ar.description"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.errors.has("ar.productDescription"),
                                expression:
                                  "errors.has('ar.productDescription')"
                              }
                            ],
                            staticClass: "text-danger text-sm"
                          },
                          [
                            _vm._v(
                              _vm._s(_vm.errors.first("ar.productDescription"))
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          staticClass: "text-danger text-sm",
                          attrs: { form: _vm.form, field: "ar.description" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  !_vm.form.quantity
                    ? _c("div", { staticClass: "vx-row" }, [
                        _c(
                          "div",
                          { staticClass: "vx-col w-full mb-2" },
                          [
                            _c(
                              "vs-switch",
                              {
                                attrs: { color: "success" },
                                model: {
                                  value: _vm.form.status,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "status", $$v)
                                  },
                                  expression: "form.status"
                                }
                              },
                              [
                                _c(
                                  "span",
                                  { attrs: { slot: "on" }, slot: "on" },
                                  [_vm._v(_vm._s(_vm.$t("common.open")))]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { attrs: { slot: "off" }, slot: "off" },
                                  [_vm._v(_vm._s(_vm.$t("common.close")))]
                                )
                              ]
                            )
                          ],
                          1
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("vs-divider"),
                  _vm._v(" "),
                  _c("div", { staticClass: "vx-row" }, [
                    _c(
                      "div",
                      { staticClass: "vx-col sm:w-2/3 w-full ml-auto" },
                      [
                        _c(
                          "vs-button",
                          {
                            staticClass: "mr-3 mb-2",
                            attrs: { type: "filled" },
                            on: {
                              click: function($event) {
                                $event.preventDefault()
                                return _vm.submitForm($event)
                              }
                            }
                          },
                          [_vm._v(_vm._s(_vm.$t("common.submit")))]
                        ),
                        _vm._v(" "),
                        _c(
                          "vs-button",
                          {
                            staticClass: "mb-2",
                            attrs: { color: "warning", type: "border" },
                            on: { click: _vm.navigateRoute }
                          },
                          [_vm._v(_vm._s(_vm.$t("common.cancel")))]
                        )
                      ],
                      1
                    )
                  ])
                ],
                1
              )
            ]
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/pages/products/single.vue":
/*!**********************************************************!*\
  !*** ./resources/js/src/views/pages/products/single.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _single_vue_vue_type_template_id_3032fa6e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./single.vue?vue&type=template&id=3032fa6e& */ "./resources/js/src/views/pages/products/single.vue?vue&type=template&id=3032fa6e&");
/* harmony import */ var _single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./single.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/products/single.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./single.vue?vue&type=style&index=0&lang=css& */ "./resources/js/src/views/pages/products/single.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _single_vue_vue_type_template_id_3032fa6e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _single_vue_vue_type_template_id_3032fa6e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/products/single.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/products/single.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/src/views/pages/products/single.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/products/single.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/products/single.vue?vue&type=style&index=0&lang=css&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/src/views/pages/products/single.vue?vue&type=style&index=0&lang=css& ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/products/single.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/pages/products/single.vue?vue&type=template&id=3032fa6e&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/src/views/pages/products/single.vue?vue&type=template&id=3032fa6e& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_3032fa6e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=template&id=3032fa6e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/products/single.vue?vue&type=template&id=3032fa6e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_3032fa6e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_3032fa6e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);