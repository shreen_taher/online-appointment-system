(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[4],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/Single.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/hosts/Single.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
/* harmony import */ var vuejs_datepicker_src_locale__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuejs-datepicker/src/locale */ "./node_modules/vuejs-datepicker/src/locale/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      activeModal: false,
      editMode: false,
      disabledDates: {},
      languages: vuejs_datepicker_src_locale__WEBPACK_IMPORTED_MODULE_1__,
      highlightedFn: {
        customPredictor: function customPredictor(date) {
          if (date.getDate() == moment().format('D')) {
            return true;
          }
        }
      },
      currentLocale: this.$store.state.currentLocale,
      form: new Form({
        first_name: '',
        last_name: '',
        email: '',
        password: '',
        role: 2,
        // attendee | student
        appointments: [{
          title: '',
          meeting_date: '',
          start_time: '',
          end_time: ''
        }]
      })
    };
  },
  created: function created() {
    Fire.$on('open-modal', this.handleForm);
  },
  computed: {
    errors: {
      get: function get() {
        return this.errors;
      },
      set: function set(error) {
        return error;
      }
    }
  },
  methods: {
    addAppointment: function addAppointment() {
      this.form.appointments.push({
        title: '',
        meeting_date: '',
        start_time: '',
        end_time: ''
      });
    },
    removeAppointment: function removeAppointment(key) {
      this.form.appointments.splice(key, 1);
    },
    openModal: function openModal() {
      this.activeModal = true;
    },
    closeModal: function closeModal() {
      this.activeModal = false;
    },
    submitForm: function submitForm() {
      var _this = this;

      this.$validator.validateAll().then(function (result) {
        if (result) {
          if (_this.editMode) _this.updateItem();else _this.createItem();
        } else {// form have errors
        }
      });
    },
    createItem: function createItem() {
      var _this2 = this;

      axios.post('api/iPanel/users', this.form) // this.form.post('api/iPanel/categories')
      .then(function (data) {
        data = data.response; //** fire event create new item */

        Fire.$emit('add-item', data);

        _this2.$vs.notify({
          color: 'success',
          title: 'success',
          text: _this2.$t('success.itemCreated'),
          position: 'top-right'
        });

        _this2.closeModal();
      }).catch(function (error) {
        //** A simple way to handle Laravel back-end validation in Vue  */
        _this2.form['errors']['errors'] = error.errors;
      });
    },
    updateItem: function updateItem() {},
    handleForm: function handleForm(data) {
      this.editMode = data.editMode;
      this.resetForm();
      if (data.item) this.form.fill(data.item);
      this.openModal();
      console.log(data);
    },
    resetForm: function resetForm() {
      this.form.clear(); //** Clear the form errors. */

      this.form.reset(); //** Reset the form fields. */
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_feather_icons__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-feather-icons */ "./node_modules/vue-feather-icons/dist/vue-feather-icons.es.js");
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    EyeIcon: vue_feather_icons__WEBPACK_IMPORTED_MODULE_0__["EyeIcon"]
  },
  name: 'CellRendererActions',
  data: function data() {
    return {
      currentLocale: this.$store.state.currentLocale
    };
  },
  methods: {
    viewModal: function viewModal() {
      var editObj = this.cloneItem(this.params.data);
      this.$router.push({
        name: 'appointments',
        params: {
          item: editObj
        }
      });
    },
    confirmDeleteRecord: function confirmDeleteRecord() {
      this.$vs.dialog({
        type: 'confirm',
        color: 'danger',
        title: "Confirm Delete",
        text: "You are about to delete \"".concat(this.params.data.first_name, "\""),
        accept: this.deleteRecord,
        acceptText: "Delete"
      });
    },
    deleteRecord: function deleteRecord() {
      var _this = this;

      axios.delete("api/iPanel/users/".concat(this.params.data.id)).then(function () {
        Fire.$emit('delete-item', _this.params.data);

        _this.showDeleteSuccess();
      }).catch(function () {
        _this.$vs.notify({
          color: 'danger',
          title: 'failed',
          position: 'top-right',
          text: 'thare is something wrong'
        });
      });
    },
    showDeleteSuccess: function showDeleteSuccess() {
      this.$vs.notify({
        color: 'success',
        title: 'Item Deleted',
        position: 'top-right',
        text: 'The selected Item deleted successfully'
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'CellRendererDate',
  computed: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  },
  name: 'CellRendererDate',
  computed: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/index.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/hosts/list/index.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ag_grid_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ag-grid-vue */ "./node_modules/ag-grid-vue/main.js");
/* harmony import */ var ag_grid_vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(ag_grid_vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _sass_vuexy_extraComponents_agGridStyleOverride_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sass/vuexy/extraComponents/agGridStyleOverride.scss */ "./resources/sass/vuexy/extraComponents/agGridStyleOverride.scss");
/* harmony import */ var _sass_vuexy_extraComponents_agGridStyleOverride_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_sass_vuexy_extraComponents_agGridStyleOverride_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _cell_renderer_CellRendererActions_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cell-renderer/CellRendererActions.vue */ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue");
/* harmony import */ var _cell_renderer_CellRendererDate_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./cell-renderer/CellRendererDate.vue */ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue");
/* harmony import */ var _cell_renderer_CellRendererNameLang_vue__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./cell-renderer/CellRendererNameLang.vue */ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue");
/* harmony import */ var _views_pages_CellRendererID_vue__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/views/pages/CellRendererID.vue */ "./resources/js/src/views/pages/CellRendererID.vue");
/* harmony import */ var _Single_vue__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Single.vue */ "./resources/js/src/views/pages/hosts/Single.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


 // Cell Renderer






/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    AgGridVue: ag_grid_vue__WEBPACK_IMPORTED_MODULE_0__["AgGridVue"],
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_2___default.a,
    // Cell Renderer
    CellRendererActions: _cell_renderer_CellRendererActions_vue__WEBPACK_IMPORTED_MODULE_3__["default"],
    CellRendererDate: _cell_renderer_CellRendererDate_vue__WEBPACK_IMPORTED_MODULE_4__["default"],
    CellRendererNameLang: _cell_renderer_CellRendererNameLang_vue__WEBPACK_IMPORTED_MODULE_5__["default"],
    CellRendererID: _views_pages_CellRendererID_vue__WEBPACK_IMPORTED_MODULE_6__["default"],
    // for create | edit
    Single: _Single_vue__WEBPACK_IMPORTED_MODULE_7__["default"]
  },
  data: function data() {
    return {
      searchQuery: '',
      collections: [],
      editMode: false,
      rowIndex: 1,
      // AgGrid
      gridApi: null,
      gridOptions: {},
      renderUniqueID: this.uniqueID(),
      defaultColDef: {
        sortable: true,
        resizable: true,
        suppressMenu: true
      },
      columnDefs: [{
        headerName: this.$t('common.id'),
        field: 'id',
        filter: true,
        width: 150,
        cellRendererFramework: 'CellRendererID',
        checkboxSelection: true,
        headerCheckboxSelectionFilteredOnly: true,
        headerCheckboxSelection: true
      }, {
        headerName: "Name",
        field: 'name',
        filter: true,
        //   cellRendererFramework: 'CellRendererNameLang',
        valueGetter: function (params) {
          return params.data.first_name + ' ' + params.data.first_name;
        }.bind(this),
        width: 200
      }, {
        headerName: "Email",
        field: 'email',
        filter: true,
        width: 150
      }, {
        headerName: this.$t('common.createdAt'),
        field: 'created_at',
        filter: true,
        cellRendererFramework: 'CellRendererDate',
        width: 200
      }, {
        headerName: this.$t('common.actions'),
        field: 'transactions',
        cellRendererFramework: 'CellRendererActions',
        width: 150
      }],
      // Cell Renderer Components
      components: {
        CellRendererActions: _cell_renderer_CellRendererActions_vue__WEBPACK_IMPORTED_MODULE_3__["default"],
        CellRendererDate: _cell_renderer_CellRendererDate_vue__WEBPACK_IMPORTED_MODULE_4__["default"],
        CellRendererNameLang: _cell_renderer_CellRendererNameLang_vue__WEBPACK_IMPORTED_MODULE_5__["default"],
        CellRendererID: _views_pages_CellRendererID_vue__WEBPACK_IMPORTED_MODULE_6__["default"]
      }
    };
  },
  watch: {
    collections: function collections(_collections) {
      this.collections = _collections;
    }
  },
  computed: {
    paginationPageSize: function paginationPageSize() {
      if (this.gridApi) return this.gridApi.paginationGetPageSize();else return 10;
    },
    totalPages: function totalPages() {
      if (this.gridApi) return this.gridApi.paginationGetTotalPages();else return 0;
    },
    currentPage: {
      get: function get() {
        if (this.gridApi) return this.gridApi.paginationGetCurrentPage() + 1;else return 1;
      },
      set: function set(val) {
        this.gridApi.paginationGoToPage(val - 1);
      }
    }
  },
  methods: {
    onGridReady: function onGridReady(params) {// this.gridApi = params.api;
      // this.columnApi = params.columnApi;
      // this.rowIndex = params.rowIndex;
      // debugger
    },
    getSelectedRows: function getSelectedRows() {
      var selectedNodes = this.gridApi.getSelectedNodes();
      var selectedData = selectedNodes.map(function (node) {
        return node.data;
      });
      var selectedDataStringPresentation = selectedData.map(function (node) {
        return node.make + ' ' + node.model;
      }).join(', ');
      alert("Selected nodes: ".concat(selectedDataStringPresentation));
    },
    random: function random() {
      return Math.floor(Math.random() * (10 - 1 + 1)) + 1;
    },
    loadcollections: function loadcollections() {
      var _this = this;

      axios.get('api/iPanel/users').then(function (_ref) {
        var response = _ref.response;
        return _this.collections = response;
      }).catch(function (error) {
        console.log('error', error);

        _this.$vs.notify({
          color: 'warning',
          title: 'Failed',
          text: error.message,
          position: 'top-right'
        });
      });
    },
    newModal: function newModal() {
      this.editMode = false;
      Fire.$emit('open-modal', {
        editMode: this.editMode
      });
    },
    deleteItem: function deleteItem(item) {
      this.collections = this.collections.filter(function (obj) {
        return obj.id != item.id;
      });
    },
    addItem: function addItem(item) {
      this.collections.unshift(item);
      console.log(this.collections);
    },
    updateItem: function updateItem(item) {
      var _this2 = this;

      this.collections.forEach(function (element, index) {
        if (element.id == item.id) {
          _this2.collections[index] = Object.assign(_this2.collections[index], item);
        }
      });
      console.log(this.collections);
      this.renderUniqueID = this.uniqueID();
    },
    setColumnFilter: function setColumnFilter(column, val) {
      var filter = this.gridApi.getFilterInstance(column);
      var modelObj = null;

      if (val !== "all") {
        modelObj = {
          type: "equals",
          filter: val
        };
      }

      filter.setModel(modelObj);
      this.gridApi.onFilterChanged();
    },
    resetColFilters: function resetColFilters() {
      // Reset Grid Filter
      this.gridApi.setFilterModel(null);
      this.gridApi.onFilterChanged();
      this.$refs.filterCard.removeRefreshAnimation();
    },
    updateSearchQuery: function updateSearchQuery(val) {
      this.gridApi.setQuickFilter(val);
    }
  },
  mounted: function mounted() {
    this.loadcollections();
    this.gridApi = this.gridOptions.api;
    /* =================================================================
      NOTE:
      Header is not aligned properly in RTL version of agGrid table.
      However, we given fix to this issue. If you want more robust solution please contact them at gitHub
    ================================================================= */

    if (this.$vs.rtl) {
      var header = this.$refs.agGridTable.$el.querySelector(".ag-header-container");
      header.style.left = "-" + String(Number(header.style.transform.slice(11, -3)) + 9) + "px";
    }
  },
  created: function created() {
    Fire.$on('update-item', this.updateItem);
    Fire.$on('add-item', this.addItem);
    Fire.$on('delete-item', this.deleteItem);
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/index.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/hosts/list/index.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#page-user-list .user-list-filters .vs__actions {\n  position: absolute;\n  top: 50%;\n}[dir] #page-user-list .user-list-filters .vs__actions {\n  -webkit-transform: translateY(-58%);\n          transform: translateY(-58%);\n}[dir=ltr] #page-user-list .user-list-filters .vs__actions {\n  right: 0;\n}[dir=rtl] #page-user-list .user-list-filters .vs__actions {\n  left: 0;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/index.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/hosts/list/index.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/index.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/Single.vue?vue&type=template&id=1c6ecca1&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/hosts/Single.vue?vue&type=template&id=1c6ecca1& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "vs-popup",
    {
      staticClass: "holamundo",
      attrs: {
        title: !_vm.editMode ? _vm.$t("common.create") : _vm.$t("common.edit"),
        active: _vm.activeModal
      },
      on: {
        "update:active": function($event) {
          _vm.activeModal = $event
        }
      }
    },
    [
      _c(
        "form",
        [
          _c("div", { staticClass: "vx-row" }, [
            _c(
              "div",
              { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
              [
                _c("vs-input", {
                  directives: [
                    {
                      name: "validate",
                      rawName: "v-validate",
                      value: "required",
                      expression: "'required'"
                    }
                  ],
                  staticClass: "w-full",
                  attrs: {
                    name: "first_name",
                    "icon-no-border": "",
                    icon: "icon icon-user",
                    "icon-pack": "feather",
                    "label-placeholder": "first name"
                  },
                  model: {
                    value: _vm.form.first_name,
                    callback: function($$v) {
                      _vm.$set(_vm.form, "first_name", $$v)
                    },
                    expression: "form.first_name"
                  }
                }),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.errors.has("first_name"),
                        expression: "errors.has('first_name')"
                      }
                    ],
                    staticClass: "text-danger text-sm",
                    attrs: { "data-vv-as": "first_name" }
                  },
                  [_vm._v(_vm._s(_vm.errors.first("first_name")))]
                ),
                _vm._v(" "),
                _c("has-error", {
                  staticClass: "text-danger text-sm",
                  attrs: { form: _vm.form, field: "first_name" }
                })
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
              [
                _c("vs-input", {
                  directives: [
                    {
                      name: "validate",
                      rawName: "v-validate",
                      value: "required",
                      expression: "'required'"
                    }
                  ],
                  staticClass: "w-full",
                  attrs: {
                    name: "last_name",
                    "icon-no-border": "",
                    icon: "icon icon-user",
                    "icon-pack": "feather",
                    "label-placeholder": "last name"
                  },
                  model: {
                    value: _vm.form.last_name,
                    callback: function($$v) {
                      _vm.$set(_vm.form, "last_name", $$v)
                    },
                    expression: "form.last_name"
                  }
                }),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.errors.has("last_name"),
                        expression: "errors.has('last_name')"
                      }
                    ],
                    staticClass: "text-danger text-sm",
                    attrs: { "data-vv-as": "last_name" }
                  },
                  [_vm._v(_vm._s(_vm.errors.first("last_name")))]
                ),
                _vm._v(" "),
                _c("has-error", {
                  staticClass: "text-danger text-sm",
                  attrs: { form: _vm.form, field: "last_name" }
                })
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "vx-row" }, [
            _c(
              "div",
              { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
              [
                _c("vs-input", {
                  directives: [
                    {
                      name: "validate",
                      rawName: "v-validate",
                      value: "required",
                      expression: "'required'"
                    }
                  ],
                  staticClass: "w-full",
                  attrs: {
                    name: "email",
                    "icon-no-border": "",
                    icon: "icon icon-user",
                    "icon-pack": "feather",
                    "label-placeholder": _vm.$t("common.email")
                  },
                  model: {
                    value: _vm.form.email,
                    callback: function($$v) {
                      _vm.$set(_vm.form, "email", $$v)
                    },
                    expression: "form.email"
                  }
                }),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.errors.has("email"),
                        expression: "errors.has('email')"
                      }
                    ],
                    staticClass: "text-danger text-sm",
                    attrs: { "data-vv-as": "email" }
                  },
                  [_vm._v(_vm._s(_vm.errors.first("email")))]
                ),
                _vm._v(" "),
                _c("has-error", {
                  staticClass: "text-danger text-sm",
                  attrs: { form: _vm.form, field: "email" }
                })
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
              [
                _c("vs-input", {
                  directives: [
                    {
                      name: "validate",
                      rawName: "v-validate",
                      value: "required",
                      expression: "'required'"
                    }
                  ],
                  staticClass: "w-full mt-6",
                  attrs: {
                    type: "password",
                    name: "password",
                    "icon-no-border": "",
                    icon: "icon icon-lock",
                    "icon-pack": "feather",
                    "label-placeholder": _vm.$t("common.password")
                  },
                  model: {
                    value: _vm.form.password,
                    callback: function($$v) {
                      _vm.$set(_vm.form, "password", $$v)
                    },
                    expression: "form.password"
                  }
                }),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.errors.has("password"),
                        expression: "errors.has('password')"
                      }
                    ],
                    staticClass: "text-danger text-sm",
                    attrs: { "data-vv-as": "password" }
                  },
                  [_vm._v(_vm._s(_vm.errors.first("password")))]
                ),
                _vm._v(" "),
                _c("has-error", {
                  staticClass: "text-danger text-sm",
                  attrs: { form: _vm.form, field: "password" }
                })
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c(
            "vx-card",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.form.appointments.length,
                  expression: "form.appointments.length"
                }
              ],
              attrs: { title: "host appointments" }
            },
            [
              _vm._l(_vm.form.appointments, function(appointment, key) {
                return _c(
                  "div",
                  { key: "experiment" + key, staticClass: "px-3" },
                  [
                    _c("div", { staticClass: "vx-row" }, [
                      _c(
                        "div",
                        { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                        [
                          _c("vs-input", {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "w-full mt-6",
                            attrs: {
                              type: "title",
                              name: "title",
                              "icon-no-border": "",
                              icon: "icon icon-lock",
                              "icon-pack": "feather",
                              "label-placeholder": "title"
                            },
                            model: {
                              value: appointment.title,
                              callback: function($$v) {
                                _vm.$set(appointment, "title", $$v)
                              },
                              expression: "appointment.title"
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "span",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.errors.has("title"),
                                  expression: "errors.has('title')"
                                }
                              ],
                              staticClass: "text-danger text-sm",
                              attrs: { "data-vv-as": "title" }
                            },
                            [_vm._v(_vm._s(_vm.errors.first("title")))]
                          ),
                          _vm._v(" "),
                          _c("has-error", {
                            staticClass: "text-danger text-sm",
                            attrs: { form: _vm.form, field: "title" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                        [
                          _c("datepicker", {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "mt-5 w-full",
                            attrs: {
                              placeholder: "meeting date",
                              highlighted: _vm.highlightedFn,
                              name: "meeting_date",
                              language: _vm.languages[_vm.currentLocale],
                              format: "d MMMM yyyy"
                            },
                            model: {
                              value: appointment.meeting_date,
                              callback: function($$v) {
                                _vm.$set(appointment, "meeting_date", $$v)
                              },
                              expression: "appointment.meeting_date"
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "span",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.errors.has("meeting_date"),
                                  expression: "errors.has('meeting_date')"
                                }
                              ],
                              staticClass: "text-danger text-sm"
                            },
                            [_vm._v(_vm._s(_vm.errors.first("meeting_date")))]
                          ),
                          _vm._v(" "),
                          _c("has-error", {
                            staticClass: "text-danger text-sm",
                            attrs: { form: _vm.form, field: "meeting_date" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "vx-row" }, [
                      _c(
                        "div",
                        { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: appointment.start_time,
                                expression: "appointment.start_time"
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "mt-5 w-full",
                            attrs: {
                              placeholder: "start time",
                              name: "start_time",
                              type: "time"
                            },
                            domProps: { value: appointment.start_time },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  appointment,
                                  "start_time",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "span",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.errors.has("start_time"),
                                  expression: "errors.has('start_time')"
                                }
                              ],
                              staticClass: "text-danger text-sm"
                            },
                            [_vm._v(_vm._s(_vm.errors.first("start_time")))]
                          ),
                          _vm._v(" "),
                          _c("has-error", {
                            staticClass: "text-danger text-sm",
                            attrs: { form: _vm.form, field: "start_time" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "vx-col sm:w-1/2 w-full mb-2" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: appointment.end_time,
                                expression: "appointment.end_time"
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "mt-5 w-full",
                            attrs: {
                              placeholder: "end time",
                              name: "end_time",
                              type: "time"
                            },
                            domProps: { value: appointment.end_time },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  appointment,
                                  "end_time",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "span",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.errors.has("end_time"),
                                  expression: "errors.has('end_time')"
                                }
                              ],
                              staticClass: "text-danger text-sm"
                            },
                            [_vm._v(_vm._s(_vm.errors.first("end_time")))]
                          ),
                          _vm._v(" "),
                          _c("has-error", {
                            staticClass: "text-danger text-sm",
                            attrs: { form: _vm.form, field: "end_time" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("vs-button", {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: key,
                          expression: "key"
                        }
                      ],
                      staticClass: "mb-4 md:mb-0",
                      staticStyle: { margin: "auto" },
                      attrs: {
                        "icon-pack": "feather",
                        type: "border",
                        color: "danger",
                        icon: "icon-trash"
                      },
                      on: {
                        click: function($event) {
                          return _vm.removeAppointment(key)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("vs-divider")
                  ],
                  1
                )
              }),
              _vm._v(" "),
              _c("vs-button", {
                staticClass: "mb-4 md:mb-0",
                staticStyle: { margin: "auto" },
                attrs: {
                  color: "primary",
                  type: "filled",
                  "icon-pack": "feather",
                  icon: "icon-plus"
                },
                on: { click: _vm.addAppointment }
              })
            ],
            2
          ),
          _vm._v(" "),
          _c("vs-divider"),
          _vm._v(" "),
          _c("div", { staticClass: "vx-row" }, [
            _c(
              "div",
              { staticClass: "vx-col sm:w-2/3 w-full ml-auto" },
              [
                _c(
                  "vs-button",
                  {
                    staticClass: "mr-3 mb-2",
                    attrs: { type: "filled" },
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.submitForm($event)
                      }
                    }
                  },
                  [_vm._v(_vm._s(_vm.$t("common.submit")))]
                ),
                _vm._v(" "),
                _c(
                  "vs-button",
                  {
                    staticClass: "mb-2",
                    attrs: { color: "warning", type: "border" },
                    on: { click: _vm.resetForm }
                  },
                  [_vm._v(_vm._s(_vm.$t("common.reset")))]
                )
              ],
              1
            )
          ])
        ],
        1
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue?vue&type=template&id=48a65a1f&":
/*!****************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue?vue&type=template&id=48a65a1f& ***!
  \****************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { style: { direction: _vm.$vs.rtl ? "rtl" : "ltr" } },
    [
      _c("eye-icon", {
        staticClass: "custom-class",
        attrs: { size: "1.5x" },
        on: { click: _vm.viewModal }
      }),
      _vm._v(" "),
      _c("feather-icon", {
        attrs: {
          icon: "Trash2Icon",
          svgClasses: "h-5 w-5 hover:text-danger cursor-pointer"
        },
        on: { click: _vm.confirmDeleteRecord }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue?vue&type=template&id=5d2b227c&":
/*!*************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue?vue&type=template&id=5d2b227c& ***!
  \*************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("vs-chip", { staticClass: "ag-grid-cell-chip" }, [
    _c("span", [_vm._v(_vm._s(_vm._f("myDate")(_vm.params.value)))])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue?vue&type=template&id=1e4d3d67&":
/*!*****************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue?vue&type=template&id=1e4d3d67& ***!
  \*****************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("vs-chip", { staticClass: "ag-grid-cell-chip" }, [
    _c("span", [
      _vm._v(
        _vm._s(_vm.params.data.first_name) +
          " " +
          _vm._s(_vm.params.data.last_name)
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/index.vue?vue&type=template&id=7f454f5a&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/hosts/list/index.vue?vue&type=template&id=7f454f5a& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "page-user-list" } },
    [
      _c(
        "div",
        { staticClass: "vx-card p-6" },
        [
          _c("div", { staticClass: "flex flex-wrap items-center" }, [
            _c(
              "div",
              { staticClass: "flex-grow" },
              [
                _c(
                  "vs-dropdown",
                  {
                    staticClass: "cursor-pointer",
                    attrs: { "vs-trigger-click": "" }
                  },
                  [
                    _c(
                      "div",
                      {
                        staticClass:
                          "p-4 border border-solid d-theme-border-grey-light rounded-full d-theme-dark-bg cursor-pointer flex items-center justify-between font-medium"
                      },
                      [
                        _c("span", { staticClass: "mr-2" }, [
                          _vm._v(
                            _vm._s(
                              _vm.currentPage * _vm.paginationPageSize -
                                (_vm.paginationPageSize - 1)
                            ) +
                              " - " +
                              _vm._s(
                                _vm.collections.length -
                                  _vm.currentPage * _vm.paginationPageSize >
                                  0
                                  ? _vm.currentPage * _vm.paginationPageSize
                                  : _vm.collections.length
                              ) +
                              " of " +
                              _vm._s(_vm.collections.length)
                          )
                        ]),
                        _vm._v(" "),
                        _c("feather-icon", {
                          attrs: {
                            icon: "ChevronDownIcon",
                            svgClasses: "h-4 w-4"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "vs-dropdown-menu",
                      [
                        _c(
                          "vs-dropdown-item",
                          {
                            on: {
                              click: function($event) {
                                return _vm.gridApi.paginationSetPageSize(10)
                              }
                            }
                          },
                          [_c("span", [_vm._v("10")])]
                        ),
                        _vm._v(" "),
                        _c(
                          "vs-dropdown-item",
                          {
                            on: {
                              click: function($event) {
                                return _vm.gridApi.paginationSetPageSize(20)
                              }
                            }
                          },
                          [_c("span", [_vm._v("20")])]
                        ),
                        _vm._v(" "),
                        _c(
                          "vs-dropdown-item",
                          {
                            on: {
                              click: function($event) {
                                return _vm.gridApi.paginationSetPageSize(25)
                              }
                            }
                          },
                          [_c("span", [_vm._v("25")])]
                        ),
                        _vm._v(" "),
                        _c(
                          "vs-dropdown-item",
                          {
                            on: {
                              click: function($event) {
                                return _vm.gridApi.paginationSetPageSize(30)
                              }
                            }
                          },
                          [_c("span", [_vm._v("30")])]
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "flex flex-wrap items-center justify-between ag-grid-table-actions-right"
              },
              [
                _c("vs-input", {
                  staticClass: "mb-4 md:mb-0 mr-4",
                  attrs: { placeholder: _vm.$t("common.search") },
                  on: { input: _vm.updateSearchQuery },
                  model: {
                    value: _vm.searchQuery,
                    callback: function($$v) {
                      _vm.searchQuery = $$v
                    },
                    expression: "searchQuery"
                  }
                }),
                _vm._v(" "),
                _c(
                  "vs-button",
                  {
                    staticClass: "mb-4 md:mb-0",
                    staticStyle: { float: "right" },
                    attrs: {
                      color: "primary",
                      type: "filled",
                      "icon-pack": "feather",
                      icon: "icon-plus"
                    },
                    on: { click: _vm.newModal }
                  },
                  [_vm._v(_vm._s(_vm.$t("common.create")))]
                )
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c("ag-grid-vue", {
            key: _vm.renderUniqueID,
            ref: "agGridTable",
            staticClass: "ag-theme-material w-100 my-4 ag-grid-table",
            attrs: {
              components: _vm.components,
              gridOptions: _vm.gridOptions,
              columnDefs: _vm.columnDefs,
              defaultColDef: _vm.defaultColDef,
              rowData: _vm.collections,
              rowSelection: "multiple",
              colResizeDefault: "shift",
              animateRows: true,
              floatingFilter: true,
              pagination: true,
              paginationPageSize: _vm.paginationPageSize,
              suppressPaginationPanel: true,
              enableRtl: _vm.$vs.rtl
            },
            on: { "grid-ready": _vm.onGridReady }
          }),
          _vm._v(" "),
          _c("vs-pagination", {
            attrs: { total: _vm.totalPages, max: 7 },
            model: {
              value: _vm.currentPage,
              callback: function($$v) {
                _vm.currentPage = $$v
              },
              expression: "currentPage"
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("Single")
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/pages/hosts/Single.vue":
/*!*******************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/Single.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Single_vue_vue_type_template_id_1c6ecca1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Single.vue?vue&type=template&id=1c6ecca1& */ "./resources/js/src/views/pages/hosts/Single.vue?vue&type=template&id=1c6ecca1&");
/* harmony import */ var _Single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Single.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/hosts/Single.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Single_vue_vue_type_template_id_1c6ecca1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Single_vue_vue_type_template_id_1c6ecca1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/hosts/Single.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/hosts/Single.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/Single.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Single.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/Single.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/hosts/Single.vue?vue&type=template&id=1c6ecca1&":
/*!**************************************************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/Single.vue?vue&type=template&id=1c6ecca1& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Single_vue_vue_type_template_id_1c6ecca1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Single.vue?vue&type=template&id=1c6ecca1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/Single.vue?vue&type=template&id=1c6ecca1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Single_vue_vue_type_template_id_1c6ecca1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Single_vue_vue_type_template_id_1c6ecca1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue":
/*!***************************************************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CellRendererActions_vue_vue_type_template_id_48a65a1f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CellRendererActions.vue?vue&type=template&id=48a65a1f& */ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue?vue&type=template&id=48a65a1f&");
/* harmony import */ var _CellRendererActions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CellRendererActions.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CellRendererActions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CellRendererActions_vue_vue_type_template_id_48a65a1f___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CellRendererActions_vue_vue_type_template_id_48a65a1f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererActions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./CellRendererActions.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererActions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue?vue&type=template&id=48a65a1f&":
/*!**********************************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue?vue&type=template&id=48a65a1f& ***!
  \**********************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererActions_vue_vue_type_template_id_48a65a1f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./CellRendererActions.vue?vue&type=template&id=48a65a1f& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererActions.vue?vue&type=template&id=48a65a1f&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererActions_vue_vue_type_template_id_48a65a1f___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererActions_vue_vue_type_template_id_48a65a1f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue":
/*!************************************************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CellRendererDate_vue_vue_type_template_id_5d2b227c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CellRendererDate.vue?vue&type=template&id=5d2b227c& */ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue?vue&type=template&id=5d2b227c&");
/* harmony import */ var _CellRendererDate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CellRendererDate.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CellRendererDate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CellRendererDate_vue_vue_type_template_id_5d2b227c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CellRendererDate_vue_vue_type_template_id_5d2b227c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererDate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./CellRendererDate.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererDate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue?vue&type=template&id=5d2b227c&":
/*!*******************************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue?vue&type=template&id=5d2b227c& ***!
  \*******************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererDate_vue_vue_type_template_id_5d2b227c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./CellRendererDate.vue?vue&type=template&id=5d2b227c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererDate.vue?vue&type=template&id=5d2b227c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererDate_vue_vue_type_template_id_5d2b227c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererDate_vue_vue_type_template_id_5d2b227c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue":
/*!****************************************************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CellRendererNameLang_vue_vue_type_template_id_1e4d3d67___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CellRendererNameLang.vue?vue&type=template&id=1e4d3d67& */ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue?vue&type=template&id=1e4d3d67&");
/* harmony import */ var _CellRendererNameLang_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CellRendererNameLang.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CellRendererNameLang_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CellRendererNameLang_vue_vue_type_template_id_1e4d3d67___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CellRendererNameLang_vue_vue_type_template_id_1e4d3d67___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererNameLang_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./CellRendererNameLang.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererNameLang_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue?vue&type=template&id=1e4d3d67&":
/*!***********************************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue?vue&type=template&id=1e4d3d67& ***!
  \***********************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererNameLang_vue_vue_type_template_id_1e4d3d67___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./CellRendererNameLang.vue?vue&type=template&id=1e4d3d67& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/cell-renderer/CellRendererNameLang.vue?vue&type=template&id=1e4d3d67&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererNameLang_vue_vue_type_template_id_1e4d3d67___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CellRendererNameLang_vue_vue_type_template_id_1e4d3d67___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/pages/hosts/list/index.vue":
/*!***********************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/list/index.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_7f454f5a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=7f454f5a& */ "./resources/js/src/views/pages/hosts/list/index.vue?vue&type=template&id=7f454f5a&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/hosts/list/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/src/views/pages/hosts/list/index.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_7f454f5a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_7f454f5a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/hosts/list/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/hosts/list/index.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/list/index.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/hosts/list/index.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/list/index.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/style-loader!../../../../../../../node_modules/css-loader!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/index.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/pages/hosts/list/index.vue?vue&type=template&id=7f454f5a&":
/*!******************************************************************************************!*\
  !*** ./resources/js/src/views/pages/hosts/list/index.vue?vue&type=template&id=7f454f5a& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_7f454f5a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=7f454f5a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/hosts/list/index.vue?vue&type=template&id=7f454f5a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_7f454f5a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_7f454f5a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);